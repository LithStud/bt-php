<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public $timestamps = false;

    public $fillable = ['name', 'lastname', 'email', 'phone'];

    public function lectures() {
        return $this->belongsToMany(Lecture::class, 'grades')
            ->using(Grade::class)
            ->withPivot('grade');
    }
}
