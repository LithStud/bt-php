<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Grade extends Pivot
{
    public $timestamps = false;
    public $incrementing = true;
    protected $table = 'grades';

    public $fillable = ['lecture_id', 'student_id', 'grade'];

    /* public function student()
    {
        return $this->hasMany(Student::class);
    }

    public function lecture()
    {
        return $this->hasMany(Lecture::class);
    } */
}
