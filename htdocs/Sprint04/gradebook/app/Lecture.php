<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
    public $timestamps = false;

    public $fillable = ['name', 'description'];

    public function students() {
        return $this->belongsToMany(Student::class, 'grades')
            ->using(Grade::class)
            ->withPivot('grade');
    }
}
