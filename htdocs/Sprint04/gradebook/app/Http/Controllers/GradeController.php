<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Lecture;
use App\Grade;

class GradeController extends Controller
{
    public function index()
    {
        $students = Student::with('lectures')->orderBy('lastname', 'ASC')->paginate();
        return view('grades.index', compact('students'));
    }

    public function grading(Student $student, Lecture $lecture)
    {
        $students = $student->orderBy('lastname')->get();
        $lectures = $lecture->orderBy('name')->get();
        return view('grades.grading', compact('students', 'lectures'));
    }

    public function save(Request $request, Student $student) {
        $validated = $request->validate([
            'student_id' => 'required|regex:/^[0-9]+/i',
            'lecture_id' => 'required|regex:/^[0-9]+/i',
            'grade' => 'required|regex:/^[0-9]+/i',
        ]);

        if ($validated) {
            $stud = $student->find($validated['student_id']);
            $stud->lectures()->attach($validated['lecture_id'], ['grade' => $validated['grade']]);
            return redirect(route('grading'))->with('status', __('Graded added'));
        }

        return redirect()->back()->withInput();
    }
}
