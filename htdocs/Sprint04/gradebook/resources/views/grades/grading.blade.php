@extends('layouts.app')

@section('content')

<div class="container">
    <h2 class="text-center mb-3">
        {{ __("Grading") }}
    </h2>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form id="grading_form" action="{{ route('grades.save') }}" method="POST">
                @csrf
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                @endif
                <div class="form-row align-items-center mb-3">
                    <div class="form-group col-md-5">
                        <label>Student:</label>
                        <select name="student_id" class="form-control">
                            <option value="" selected>{{ __('Select Student') }}</option>
                            @foreach ($students as $student)
                            <option value="{{ $student->id }}">{{ $student->lastname }} {{ $student->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-5">
                        <label>Lecture:</label>
                        <select name="lecture_id" class="form-control">
                            <option value="" selected>{{ __('Select Lecture') }}</option>
                            @foreach ($lectures as $lecture)
                            <option value="{{ $lecture->id }}">{{ $lecture->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Grade</label>
                        <input name="grade" type="number" min="0" class="form-control">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
</div>

@endsection
