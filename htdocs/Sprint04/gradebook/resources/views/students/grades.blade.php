@extends('layouts.app')

@section('content')

<div class="container">
    <h2 class="text-center mb-3">Student: {{$stud->name}} {{$stud->lastname}} Grades</h2>
    <div class="row justify-content-center">

        <table class="table table-hover table-striped table-dark">
            <thead>
                <tr>
                    <th>Lecture</th>
                    <th>Grades</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($grades as $name => $data)
                <tr>
                    <td>{{ $name }}</td>
                    <td>
                        <ul class="pagination">
                            @foreach ($data as $lectureInfo)
                            <li class="page-item page-link">{{ $lectureInfo->pivot->grade }}</li>
                            @endforeach
                        </ul>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="2">{{ __('No grades found.') }}</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>

@endsection
