@extends('layouts.app')

@section('content')

<div class="container">
    <h2 class="text-center mb-3">{{ __('Student List') }}</h2>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="btn-toolbar justify-content-between mb-3" role="toolbar" aria-label="Toolbar">
                <div class="btn-group" role="group" aria-label="First group">
                    <a href="{{ route('students.add') }}" class="btn btn-success"
                        role="button">{{ __('Add Student') }}</a>
                </div>
                <form class="input-group" type="POST" action="{{ route('students') }}">
                    <input name="search_term" type="text" class="form-control" placeholder="Name or Lastname"
                        aria-label="Name or Lastname" aria-describedby="basic-addon2" @if ($searchFilter)
                        value="{{ $searchFilter }}" @endif>
                    <div class="input-group-append">
                        <button class="btn btn-outline-success" type="submit">{{ __('Search') }}</button>
                    </div>
                </form>
            </div>
            <div class="paginator-container">
                {{ $students->links() }}
            </div>
            <table id="students_table_list" class="table table-hover table-striped table-dark">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Lastname</th>
                        <th scope="col">Name</th>
                        <th scope="col">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($students as $student)
                    <tr>
                        <th scope="row">{{$student->id}}</th>
                        <td>{{$student->lastname}}</td>
                        <td>{{$student->name}}</td>
                        <td>
                            <div class="btn-group-sm" role="group">
                                <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    {{ __('Options') }}
                                </button>
                                <div class="dropdown-menu dropdown-dark" aria-labelledby="btnGroupDrop1">
                                    <a class="dropdown-item"
                                        href="{{ route('students.grades', ['id' => $student->id]) }}" role="button">Show
                                        Grades</a>
                                    <a class="dropdown-item" data-action="details" data-student-id="{{$student->id}}"
                                        href="#" role="button">Details</a>
                                    <a class="dropdown-item" href="{{ route('students.edit', ["id"=>$student->id]) }}"
                                        role="button">Edit</a>
                                    <a class="dropdown-item" data-action="delete" data-student-id="{{$student->id}}"
                                        href="#" role="button">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <th colspan="4">{{ __('No students found.') }}</th>
                    </tr>
                    @endforelse
                </tbody>
            </table>

            <div class="paginator-container">
                {{ $students->links() }}
            </div>
        </div>
    </div>
</div>

<!-- Details Modal -->
<div class="modal fade" id="student_details_modal" tabindex="-1" role="dialog" aria-labelledby="StudentDetails"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">ID: <span id="student_id"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="list-group">
                    <li class="list-group-item">Name: <span id="student_name"></span></li>
                    <li class="list-group-item">Lastname: <span id="student_lastname"></span></li>
                    <li class="list-group-item">Email: <span id="student_email"></span></li>
                    <li class="list-group-item">Phone: <span id="student_phone"></span></li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Deletion Form (hidden) -->
<form id="deleteForm" action="{{ route('students.delete') }}" method="POST" style="display: hidden;">
    @csrf
    @method('DELETE')
    <input id="delete_id" type="hidden" name="delete[]" value="">
</form>

@endsection

@section('scripts')
<script>
    document.addEventListener('DOMContentLoaded', e => {
        let students = {!! json_encode($students) !!}.data;
        //console.log(students);

        document.getElementById('students_table_list').addEventListener('click', e => {
            if (e.target.dataset && e.target.dataset['action']) {
                e.preventDefault();
                //console.log('Action: ', e.target.dataset['action'], ' for ID: ', e.target.dataset['studentId']);
                switch (e.target.dataset['action']) {
                    case 'delete':
                        if(confirm('Confirm student deletion? This action is irreversible')) {
                            console.log('Deleting...');
                            let form = document.getElementById('deleteForm');
                            document.getElementById('delete_id').value = e.target.dataset['studentId'];
                            //form.getElementById('delete_id').value = e.target.dataset['studentId'];
                            form.submit();
                        }
                        break;

                    case 'details':
                        if(fillModal(parseInt(e.target.dataset['studentId']))) {
                            $('#student_details_modal').modal('show');
                        }
                        break;

                    default:
                        break;
                }
            }
        });

        function fillModal(id) {
            if (!students || students.lenght == 0) {
                return false;
            }

            let modal = {
                id: document.getElementById('student_id'),
                name: document.getElementById('student_name'),
                lastname: document.getElementById('student_lastname'),
                email: document.getElementById('student_email'),
                phone: document.getElementById('student_phone'),
            }

            let student = students.find(student => {return student.id === id});

            if (student) {
                modal.id.innerText = student.id;
                modal.name.innerText = student.name;
                modal.lastname.innerText = student.lastname;
                modal.email.innerText = student.email;
                modal.phone.innerText = student.phone;
                return true;
            }

            return false;
        }
    });
</script>
@endsection
