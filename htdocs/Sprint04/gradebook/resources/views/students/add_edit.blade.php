@extends('layouts.app')

@section('content')
<div class="container">
    <h2 class="text-center mb-3">
        @if ($edit)
        {{ __("Edit Student") }}
        @else
        {{ __("Add New Student") }}
        @endif
    </h2>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <form @if ($edit) action="{{ route('students.save.id', ["id"=>$student->id]) }}" @else
                action="{{ route('students.save') }}" @endif method="POST">
                @csrf
                @if ($edit)
                @method('PUT')
                @endif
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="form-group row">
                    <label for="stud_name" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" class="form-control" id="stud_name" placeholder="Name" @if($edit)
                            value="{{$student->name}}" @endif>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="stu_lastname" class="col-sm-2 col-form-label">Last Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="lastname" class="form-control" id="stu_lastname"
                            placeholder="Last Name" @if($edit) value="{{$student->lastname}}" @endif>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="stud_email" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input name="email" type="email" class="form-control" id="stud_email" placeholder="Email"
                            @if($edit) value="{{$student->email}}" @endif>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="stud_phone" class="col-sm-2 col-form-label">Phone</label>
                    <div class="col-sm-10">
                        <input name="phone" type="phone" class="form-control" id="stud_phone" placeholder="Phone #"
                            @if($edit) value="{{$student->phone}}" @endif>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">
                            @if ($edit)
                            {{ __('Edit') }}
                            @else
                            {{ __('Add') }}
                            @endif
                        </button>
                        <a href="{{ route('students') }}" class="btn btn-primary">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
