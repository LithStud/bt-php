@extends('layouts.app')

@section('custom_css')
<link href="{{ asset('css/summernote-bs4.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <h2 class="text-center mb-3">
        @if ($edit)
        {{ __("Edit Lecture") }}
        @else
        {{ __("Add New Lecture") }}
        @endif
    </h2>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <form @if ($edit) action="{{ route('lectures.save.id', ["id"=>$lecture->id]) }}" @else
                action="{{ route('lectures.save') }}" @endif method="POST">
                @csrf
                @if ($edit)
                @method('PUT')
                @endif
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="form-group row">
                    <label for="lec_name" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" class="form-control" id="lec_name" placeholder="Name" @if($edit)
                            value="{{$lecture->name}}" @endif>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="stu_lastname" class="col-sm-2 col-form-label">Last Name</label>
                    <div class="col-sm-10">
                        <textarea id="summernote" class="d-none"
                            name="description">@if($edit) {{$lecture->description}} @endif</textarea>
                        <!-- Loading indicator before showing summertime editor -->
                        <div id="custom_loader_anim" class="gooey">
                            <span class="dot"></span>
                            <div class="dots">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">
                            @if ($edit)
                            {{ __('Edit') }}
                            @else
                            {{ __('Add') }}
                            @endif
                        </button>
                        <a href="{{ route('lectures') }}" class="btn btn-primary">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script defer src="{{ asset('js/summernote-bs4.min.js') }}"></script>
<script>
    document.addEventListener('DOMContentLoaded', e => {
        $('#summernote').summernote({
            codeviewFilter: true,
            codeviewIframeFilter: true,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ],
            callbacks: {
                onInit: function() {
                    console.log('Summernote is launched');
                    document.getElementById('custom_loader_anim').classList.add('d-none');
                }
            }
        });
    });
</script>
@endsection
