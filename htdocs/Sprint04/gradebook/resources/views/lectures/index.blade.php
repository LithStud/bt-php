@extends('layouts.app')

@section('content')

<div class="container">
    <h2 class="text-center mb-3">{{ __('Lecture List') }}</h2>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="btn-toolbar justify-content-between mb-3" role="toolbar" aria-label="Toolbar">
                <div class="btn-group" role="group" aria-label="First group">
                    <a href="{{ route('lectures.add') }}" class="btn btn-success"
                        role="button">{{ __('Add Lecture') }}</a>
                </div>
                <form class="input-group" type="POST" action="{{ route('lectures') }}">
                    <input name="search_term" type="text" class="form-control" placeholder="Name"
                        aria-label="Name" aria-describedby="basic-addon2" @if ($searchFilter)
                        value="{{ $searchFilter }}" @endif>
                    <div class="input-group-append">
                        <button class="btn btn-outline-success" type="submit">{{ __('Search') }}</button>
                    </div>
                </form>
            </div>
            <div class="paginator-container">
                {{ $lectures->links() }}
            </div>
            <table id="lectures_table_list" class="table table-hover table-striped table-dark">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Description</th>
                        <th scope="col">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($lectures as $lecture)
                    <tr>
                        <th scope="row">{{$lecture->id}}</th>
                        <td>{{$lecture->name}}</td>

                        <td>{{ Str::limit(strip_tags($lecture->description), 50, '...') }}</td>
                        <td>
                            <div class="btn-group-sm" role="group">
                                <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    {{ __('Options') }}
                                </button>
                                <div class="dropdown-menu dropdown-dark" aria-labelledby="btnGroupDrop1">
                                    <a class="dropdown-item" data-action="details" data-lecture-id="{{$lecture->id}}"
                                        href="#" role="button">Details</a>
                                    <a class="dropdown-item" href="{{ route('lectures.edit', ["id"=>$lecture->id]) }}"
                                        role="button">Edit</a>
                                    <a class="dropdown-item" data-action="delete" data-lecture-id="{{$lecture->id}}"
                                        href="#" role="button">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <th colspan="4">{{ __('No lectures found.') }}</th>
                    </tr>
                    @endforelse
                </tbody>
            </table>

            <div class="paginator-container">
                {{ $lectures->links() }}
            </div>
        </div>
    </div>
</div>

<!-- Details Modal -->
<div class="modal fade" id="lecture_details_modal" tabindex="-1" role="dialog" aria-labelledby="LectureDetails"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">ID: <span id="lecture_id"></span> - <span id="lecture_name"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="list-group">
                    <li class="list-group-item" id="lecture_description"></li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Deletion Form (hidden) -->
<form id="deleteForm" action="{{ route('lectures.delete') }}" method="POST" style="display: hidden;">
    @csrf
    @method('DELETE')
    <input id="delete_id" type="hidden" name="delete[]" value="">
</form>

@endsection

@section('scripts')
<script>
    document.addEventListener('DOMContentLoaded', e => {
        let lectures = {!! json_encode($lectures) !!}.data;
        //console.log(lectures);

        document.getElementById('lectures_table_list').addEventListener('click', e => {
            if (e.target.dataset && e.target.dataset['action']) {
                e.preventDefault();
                //console.log('Action: ', e.target.dataset['action'], ' for ID: ', e.target.dataset['studentId']);
                switch (e.target.dataset['action']) {
                    case 'delete':
                        if(confirm('Confirm lecture deletion? This action is irreversible')) {
                            console.log('Deleting...');
                            let form = document.getElementById('deleteForm');
                            document.getElementById('delete_id').value = e.target.dataset['lectureId'];
                            //form.getElementById('delete_id').value = e.target.dataset['studentId'];
                            form.submit();
                        }
                        break;

                    case 'details':
                        if(fillModal(parseInt(e.target.dataset['lectureId']))) {
                            $('#lecture_details_modal').modal('show');
                        }
                        break;

                    default:
                        break;
                }
            }
        });

        function fillModal(id) {
            if (!lectures || lectures.lenght == 0) {
                return false;
            }

            let modal = {
                id: document.getElementById('lecture_id'),
                name: document.getElementById('lecture_name'),
                description: document.getElementById('lecture_description'),
            }

            let lecture = lectures.find(lecture => {return lecture.id === id});

            if (lecture) {
                modal.id.innerText = lecture.id;
                modal.name.innerText = lecture.name;
                modal.description.innerHTML = lecture.description;
                return true;
            }

            return false;
        }
    });
</script>
@endsection
