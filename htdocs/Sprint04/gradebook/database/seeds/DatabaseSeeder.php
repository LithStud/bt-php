<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        // $this->call(UsersTableSeeder::class);
        $lectures = factory('App\Lecture', config('gradebook.seeder.lectureCount'))->create();
        $students = factory('App\Student', config('gradebook.seeder.studentCount'))->create();
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@net',
            'password' => Hash::make('admin'),
        ]);
        factory('App\Grade', config('gradebook.seeder.gradeCount'))->create();
    }
}
