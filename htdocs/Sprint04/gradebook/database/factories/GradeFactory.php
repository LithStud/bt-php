<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Grade;
use Faker\Generator as Faker;

$factory->define(Grade::class, function (Faker $faker) {
    return [
        'lecture_id' => $faker->numberBetween(1, config('gradebook.seeder.lectureCount')),
        'student_id' => $faker->numberBetween(1, config('gradebook.seeder.studentCount')),
        'grade' => $faker->numberBetween(1, 10),
    ];
});
