<?php
return [
    'seeder' => [
        'lectureCount' => 32,
        'studentCount' => 60,
        'gradeCount' => 80,
    ],
];
