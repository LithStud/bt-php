<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory('App\Task', 17)->create();
        factory('App\User', 5)->create();
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@net',
            'password' => Hash::make('admin'),
        ]);
    }
}
