<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Task;
use Faker\Generator as Faker;
use App\Status;

$factory->define(Task::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'description' => $faker->paragraph(rand(1, 4)),
        'status_id' => function () {
            return factory(Status::class)->create()->id;
        },
    ];
});
