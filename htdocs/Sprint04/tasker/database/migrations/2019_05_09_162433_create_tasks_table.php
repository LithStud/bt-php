<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            // engine, charset
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_lithuanian_ci';

            $table->bigIncrements('id');
            $table->string('name', 128);
            $table->text('description')->nullable();
            $table->unsignedTinyInteger('status_id')->nullable();
            $table->dateTime('add_date')->default(now());
            $table->dateTime('completed_date')->nullable();

            // add foreign key
            $table->foreign('status_id')
                ->references('id')->on('statuses')
                ->onDelete('SET NULL')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
