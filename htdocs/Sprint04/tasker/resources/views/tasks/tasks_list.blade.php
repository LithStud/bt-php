@extends('layouts.app')

@section('content')

<div class="container justify-content-center">
    <form id="status_filter_form" action="{{ route('tasks') }}" method="get" style="display: none;">
        <input type="hidden" name="status_filter" id="status_filter_value">
    </form>
    <h1>{{ __('Tasks') }}</h1>
    @auth
    <form action="{{ route('tasks.delete') }}" method="POST" id="delete_task_form">
        @method('DELETE')
        @csrf
        @endauth
        <table class="table table-dark">
            <thead>
                @auth
                <tr>
                    <td colspan="2">
                        <div class="input-group">
                            <button class="form-control btn-danger" type="submit">DELETE</button>
                        </div>
                    </td>
                    <td colspan="4">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Filter:</span>
                            </div>
                            <select class="form-control" id="status_filter_selection">
                                <option value="" selected>{{ __('Show All') }}</option>
                                @foreach ($statuses as $status)
                                <option value="{{ $status->id }}" @if ($status->id == session('status_filter'))
                                    selected
                                    @endif>{{ $status->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </td>
                </tr>
                @endauth
                <tr>
                    @auth
                    <th scope="col">#</th>
                    @endauth
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Desc.</th>
                    <th scope="col">Created</th>
                    <th scope="col">Status</th>
                </tr>
            </thead>
            <tbody>



                @foreach ($tasks as $task)
                <tr>
                    @auth
                    <td scope="row"><input type="checkbox" name="delete[]" value="{{ $task->id }}"></td>
                    @endauth
                    <td scope="row">{{ $task->id }}</td>
                    <td><a href="{{ route('tasks.show', ['id' => $task->id]) }}">{{ $task->name }}</a></td>
                    <td>{{ Str::limit($task->description, 50, '...') }}</td>
                    <td>{{ $task->add_date }}</td>
                    <td>{{ $task->status->name }}</td>
                </tr>
                @endforeach

            </tbody>
        </table>
        @auth
    </form>
    @endauth
    {{ $tasks->links('paginator.default') }}
</div>

@endsection

@section('scripts')
<script>
    document.addEventListener('DOMContentLoaded', e => {
        document.getElementById('status_filter_selection').addEventListener('change', e => {
            document.getElementById('status_filter_value').value = e.target.value;
            document.getElementById('status_filter_form').submit();
        });
    });
</script>
@endsection
