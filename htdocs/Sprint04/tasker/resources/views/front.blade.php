@extends('layouts.app')

@section('content')
<div class="container">
    <div class="jumbotron">
        <h1 class="display-4 text-center">Tasker</h1>
        <p class="lead">Welcome to the Tasker application.</p>
        <hr class="my-4">
        <p class="lead text-center">
            <a class="btn btn-primary btn-lg" href="{{ route('tasks') }}" role="button">Start</a>
        </p>
    </div>
</div>
@endsection


