<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('front');
});

//Auth::routes();
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/tasks', 'TasksController@index')->name('tasks');
Route::get('/tasks/{id}/show', 'TasksController@show')->name('tasks.show')->where('id', '[0-9]+');

Route::middleware('auth')->group(function () {
    Route::get('/tasks/add')->name('tasks.add'); // new form
    Route::get('/tasks/{id}/edit')->name('tasks.edit'); // edit form

    Route::post('/tasks/save')->name('tasks.save'); // new
    Route::put('/tasks/{id}/save')->name('tasks.save'); // edit
    Route::delete('/tasks/delete', 'TasksController@delete')->name('tasks.delete'); // delete
});
