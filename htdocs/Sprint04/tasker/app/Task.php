<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public $timestamps = false;

    protected $fillable = ['name', 'description', 'status_id', 'add_date', 'completed_date'];

    public function status() {
        return $this->belongsTo(Status::class);
    }
}
