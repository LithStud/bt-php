<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Status;

class TasksController extends Controller
{
    public function index(Request $request)
    {
        $statusFilter = $request->input('status_filter', session()->get('status_filter', ''));

        session()->put('status_filter', $statusFilter);

        $tasks = Task::with('status')
            ->when($statusFilter, function ($q) use ($statusFilter) {
                $q->where('status_id', $statusFilter);
            })
            ->orderBy('add_date', 'DESC')
            ->paginate(10);

        if ($tasks->count() == 0 && session()->get('status_filter', '')) {
            session()->put('status_filter', '');
            return redirect(route('tasks'));
        }
        $statuses = Status::has('task')->orderBy('name', 'ASC')->get();

        return view('tasks.tasks_list', compact('tasks', 'statuses'));
    }

    public function show($id)
    {
        $task = Task::with('status')->findOrFail($id);
        return view('tasks.tasks_show', compact('task'));
    }

    public function delete(Request $request)
    {
        $toDelete = $request->input('delete', []);

        if (!empty($toDelete)) {
            Task::destroy($toDelete);
        }

        return redirect(route('tasks'));
    }
}
