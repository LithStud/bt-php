<?php
return [
    'paginator' => [
        'books' => 10,
        'authors' => 5
    ],

    'app' => [
        'title' => 'Library'
    ],

    'locale' => [
        'list' => ['lt', 'en'],
        'default' => 'lt'
    ]
];
