<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Book;
use Faker\Generator as Faker;
use App\Author;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(rand(1,3), true),
        'pages' => $faker->numberBetween(50, 500),
        'description' => $faker->paragraph(rand(1, 4)),
        'isbn' => $faker->isbn13,
        'author_id' => function () {
            return factory(Author::class)->create()->id;
        },
    ];
});
