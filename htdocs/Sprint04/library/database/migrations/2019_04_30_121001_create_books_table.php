<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            // engine, charset
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_lithuanian_ci';

            // table cols
            $table->bigIncrements('id');
            $table->string('title', 128)->index()->nullable(false);
            $table->integer('pages')->unsigned();
            $table->text('description');
            $table->string('isbn', 17)->unique()->nullable(false);
            $table->unsignedBigInteger('author_id')->nullable(false);

            // add foreign key
            $table->foreign('author_id')
                ->references('id')->on('authors')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
