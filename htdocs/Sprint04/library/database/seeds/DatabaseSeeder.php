<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory('App\Book', 32)->create();
        factory('App\User', 13)->create();
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@net',
            'password' => Hash::make('admin'),
            'role' => 1,
        ]);
    }
}
