<?php
use Illuminate\Support\Facades\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Authorisation, if full functionality uncoment line 1 and coment the rest
//Auth::routes();
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');


Route::get('/books', 'BookController@listing')->name('books');
Route::post('/books', 'BookController@listing')->name('booksWithFilter');
Route::get('/books/filterReset', 'BookController@filterReset')->name('booksFilterReset');
Route::get('/books/{id}', 'BookController@show')->name('books.show')->where('id', '[0-9]+');
Route::get('/filter', 'BookController@filteredList')->name('booksFilter');

Route::middleware('auth')->group(function(){
    Route::post('/books/add', 'BookController@add')->name('books.save');
    Route::put('/books/{id}/edit', 'BookController@edit')->name('books.edit')->where('id', '[0-9]+');
    //Route::delete('/books/{id}/edit', 'BookController@edit')->name('books.edit')->where('id', '[0-9]+');
    Route::delete('/books/delete', 'BookController@delete')->name('books.delete')->middleware('can:trash,App\Book');
});
Route::get('/authors', 'BookController@listing')->name('authors');

Route::post('/lang', function() {
    session()->put('lang', Request::input('lang'));
    return redirect()->back();
})->name('setLang')->middleware('lang');
