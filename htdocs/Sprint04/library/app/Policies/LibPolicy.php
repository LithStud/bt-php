<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LibPolicy
{
    use HandlesAuthorization;

    public function updateOrNew(User $user) {
        return (($user->role == 'admin') || ($user->role == 'user'));
    }

    public function trash(User $user) {
        return $user->role == 'admin';
    }
}
