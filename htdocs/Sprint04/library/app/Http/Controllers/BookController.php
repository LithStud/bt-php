<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stevebauman\Purify\Facades\Purify;
use App\Book;
use App\Author;

class BookController extends Controller
{
    public function filterReset(Book $book)
    {
        session()->put('authorFilter', '');
        session()->put('orderByFilter', $book->getSortable()[0]);
        session()->put('orderFilter', 'DESC');
        session()->put('searchTerm', '');

        return redirect(route('books'));
    }

    public function listing(Request $request, Book $book, Author $author)
    {
        //session()->put('orderFilter', 'DESC');
        $authors = $author->orderBy('name', 'ASC')->has('Book')->get();

        $sortList = $book->getSortable();

        $authorFilter = $request->input('authorFilter', session()->get('authorFilter', ''));
        $orderByFilter = $request->input(
            'orderByFilter',
            session()->get('orderByFilter', $book->getSortable()[0])
        );
        // checkbox has value set as ASC so if not checked default should be DESC
        $orderFilter = $request->input('orderFilter', 'DESC');

        $searchTerm = $request->input('searchTerm', session()->get('searchTerm', ''));

        session()->put('authorFilter', $authorFilter);
        session()->put('orderByFilter', $orderByFilter);
        session()->put('orderFilter', $orderFilter);
        session()->put('searchTerm', $searchTerm);

        $books = $book->with('author')
            ->when($authorFilter, function ($q) use ($authorFilter) {
                $q->whereHas('author', function ($q1) use ($authorFilter) {
                    $q1->where('id', $authorFilter);
                });
            })
            ->when($searchTerm, function ($q3) use ($searchTerm) {
                $q3->where('title', 'LIKE', "%$searchTerm%")
                    ->orWhere('isbn', 'LIKE', "%$searchTerm%");
            })
            ->when($orderByFilter, function ($q2) use ($orderByFilter, $orderFilter) {
                $q2->orderBy($orderByFilter, $orderFilter);
            })
            ->paginate(config('library.paginator.books'));

        return view('books.books_listing', compact('books', 'authors', 'sortList'));
    }

    public function filteredList(Request $request, Book $book, Author $author)
    {
        $authors = $author->orderBy('name', 'ASC')->has('Book')->get();

        $authorFilter = 1;

        $books = $book->with('author')
            ->when($authorFilter, function ($q) use ($authorFilter) {
                $q->whereHas('author', function ($q1) use ($authorFilter) {
                    $q1->where('id', $authorFilter);
                });
            })
            ->paginate(config('library.paginator.books'));

        return view('books.books_listing', compact('books', 'authors'));
    }

    public function show($id, Book $book)
    {
        $result = $book->with('Author')->findOrFail($id);
        return view('books.book_show ',  ['book' => $result]);
    }

    public function delete(Request $request, Book $book)
    {
        $delete = $request->input('delete', []);

        if ($delete) {
            $book->destroy($delete);
        }
        //dump($delete);
        return redirect()->back();
    }

    public function add(Request $request)
    {
        $validated = $request->validate([
            'title' => 'string|required|max:255',
            'pages' => 'integer|required',
            'isbn' => 'integer|required',
            'description' => 'string|required',
            'author_id' => 'integer|nullable',
        ]);
        return redirect()->back()->withInput();
    }

    public function edit()
    {
        //
    }

    public function save($id = null, Book $book, Request $request)
    {
        $validated = $request->validate([
            'title' => 'string|required|max:255',
            'pages' => 'integer|required',
            'isbn' => 'integer|required',
            'description' => 'string|required',
            'author_id' => 'integer|nullable',
        ]);

        if ($validated) {
            $validated['description'] = Purify::clean($validated['description']);
            $book->updateOrCreate(['id' => $id], $validated);
            return redirect(route('books'));
        }

        return redirect()->back()->withInput();
    }
}
