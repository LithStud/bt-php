<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public $fillable = ['title', 'pages', 'description', 'isbn', 'author_id'];

    protected $sortable = [
        'id',
        'title',
        'pages',
        'isbn',
    ];

    public $timestamps = false;

    public function author()
    {
        return $this->belongsTo(Author::class);
    }

    public function getSortable()
    {
        return $this->sortable;
    }
}
