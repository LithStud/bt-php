<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    public $fillable = ['name', 'lastname'];

    public $timestamps = false;

    public function book()
    {
        return $this->hasMany(Book::class);
    }
}
