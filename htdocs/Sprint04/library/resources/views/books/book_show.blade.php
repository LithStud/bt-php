@extends('layouts.app')

@section('content')
<div class="container mb-2 mt-2">
    <div class="row card">
        <div class="card-header">{{$book->title}} by <b><i>{{ $book->author->name }} {{ $book->author->lastname }}</i></b></div>
        <div class="card-body">{{$book->description}}</div>
        <div class="card-footer">ISBN: {{$book->isbn}}, pages: {{$book->pages}}</div>
    </div>
    <div class="row mt-2"><a class="btn btn-success" href="{{ url()->previous() }}">Back</a></div>
</div>
@endsection
