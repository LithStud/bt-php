@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row col-md-12 justify-content-center">{{ $books->links() }}</div>
    <div class="row col-md-12 mb-2 collapse justify-content-center" id="filterCard">
        <form action="{{ route('booksWithFilter') }}" method="post">
            @csrf
            <div class="card card-body">
                <div class="input-group">

                    <div class="input-group-prepend">
                        <label class="input-group-text" for="authorFilter">Author:</label>
                    </div>
                    <select class="custom-select" name="authorFilter" id="authorFilter">
                        <option value="">{{ __('All') }}</option>
                        @foreach ($authors as $author)
                        <option value="{{ $author->id }}" @if (session('authorFilter')===$author->id) selected @endif>
                            {{ $author->name }} {{ $author->lastname }}
                        </option>
                        @endforeach
                    </select>

                    <div class="input-group-prepend ml-2">
                        <label class="input-group-text" for="orderByFilter">Order By:</label>
                    </div>
                    <select class="custom-select text-capitalize" name="orderByFilter" id="orderByFilter">
                        @foreach ($sortList as $sort)
                        <option value="{{ $sort }}" @if (session('orderByFilter')===$sort) selected @endif>
                            {{ $sort }}
                        </option>
                        @endforeach
                    </select>

                    <div class="input-group-prepend ml-2">
                        <div class="input-group-text">
                            <input value="ASC" id="orderFilter" name="orderFilter" type="checkbox"
                                aria-label="Checkbox for list order" @if (session('orderFilter')==='ASC' ) checked
                                @endif>
                        </div>
                        <label class="input-group-text" for="orderFilter">
                            ASC</label>
                    </div>

                    <div class="input-group-prepend ml-2">

                    </div>

                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-success" type="submit">
                    {{ __('Filter') }}
                </button>
                <a class="btn btn-warning" href="{{ route('booksFilterReset') }}">
                    {{ __('Clear Filters') }}
                </a>
            </div>
        </form>
    </div>

    <div class="row col-md-12 mb-2 justify-content-between">
        @auth
        <div>
            <button id="delete_btn" type="submit" class="btn btn-danger">{{ __('Delete') }}</button>
            <button type="button" class="btn btn-primary ml-2" data-toggle="modal"
                data-target="#addBookModal">{{ __('Add') }}</button>
        </div>
        @endauth
        <div>
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#filterCard"
                aria-expanded="false" aria-controls="filterCard">
                {{ __('Filters') }}
            </button>
        </div>
        <form action="{{ route('booksWithFilter') }}" method="post">
            @csrf
            <div class="input-group mb-3">
                <input type="text" class="form-control" name="searchTerm" placeholder="{{ __('Title, ISBN') }}"
                    aria-label="{{ __('Title, ISBN') }}" aria-describedby="basic-addon2"
                    value="{{ session('searchTerm', '') }}">
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="submit">{{ __('Search') }}</button>
                </div>
            </div>
        </form>
    </div>
    <div class="row col-md-12 justify-content-center table-responsive">
        @auth
        <form class="justify-content-center" action="{{ route('books.delete') }}" method="post" id="delete-form">
            @csrf
            @method('DELETE')
            @endauth
            <table class="table table-hover table-dark">
                <thead>
                    <tr>
                        @auth
                        <th scope="col"><input id="check_all" type="checkbox" name="check_all"></th>
                        @endauth
                        <th scope="col">{{ __('ID') }}</th>
                        <th scope="col">{{ __('Title') }}</th>
                        <th scope="col">{{ __('Author') }}</th>
                        <th scope="col">{{ __('ISBN') }}</th>
                        <th scope="col">{{ __('Pages') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($books as $book)
                    <tr>
                        @auth
                        <th scope="row"><input type="checkbox" name="delete[]" value="{{ $book->id }}"></th>
                        @endauth
                        <th scope="row">{{ $book->id }}</th>
                        <td><a href="{{ route('books.show', ['id'=> $book->id]) }}">{{ $book->title }}</a></td>
                        <td>{{ $book->author->name }} {{ $book->author->lastname }}</td>
                        <td>{{ $book->isbn }}</td>
                        <td>{{ $book->pages }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
    </div>
    @auth
    </form>
    @endauth
    <div class="row col-md-12 justify-content-center">{{ $books->links() }}</div>
</div>

<!-- Modal -->
<div class="modal fade" id="addBookModal" tabindex="-1" role="dialog" aria-labelledby="addBookModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addBookModalLabel">{{ __('Add new Book') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="add_book_form" action="{{ route('books.save') }}" method="post">
                    @csrf
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="book_title">Title</span>
                        </div>
                        <input type="text" class="form-control" aria-label="Title"
                            aria-describedby="book_title">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="book_isbn">ISBN</span>
                        </div>
                        <input type="text" class="form-control" aria-label="ISBN"
                            aria-describedby="book_isbn">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="book_pages">Pages</span>
                        </div>
                        <input type="number" min="0" class="form-control" aria-label="Pages"
                            aria-describedby="book_pages">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="add_book_btn" type="button" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@auth

<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', e => {
    @if ($errors->any())
        $('#addBookModal').modal('show');
    @endif

    document.getElementById('check_all').addEventListener('change', e => {
        e.preventDefault();
        Array.from(document.getElementById('delete-form').getElementsByTagName('input'))
            .forEach(checkbox => {
                if(checkbox.name == 'delete[]') {
                    checkbox.checked = !checkbox.checked;
                }
            });
    });

    document.getElementById('delete_btn').addEventListener('click', e => {
        e.preventDefault();
        let form = document.getElementById('delete-form');
        let hasChecked = Array.from(form.getElementsByTagName('input'))
            .some(checkbox => (checkbox.name == 'delete[]' && checkbox.checked));
        if (!hasChecked) {
            alert('{{ __("Nothing selected for deletion") }}');
            return;
        }
        form.submit();
    });

    document.getElementById('add_book_btn').addEventListener('click', e => {
        e.preventDefault();
        document.getElementById('add_book_form').submit();
    });
});
</script>
@endauth
@endsection
