//mode body tag 100% document height
//document.getElementsByTagName('body')[0].style = `height: ${window.innerHeight}px`;

let token = document.querySelector('meta[name=csrf-token]').content;

let model = {
    staff: [],
    projects: [],
    departments: null,
}

let UI = {
    modalEl: document.getElementById('modal'),
    staffEl: document.getElementById('staff'),
    personProjectsEl: document.getElementById('projects_data'),
    editPersonFormEl: document.getElementById('edit_person'),
    editAssignmentsBtn: document.getElementById('edit_assignments_btn'),
    addPersonFormEl: document.getElementById('add_person'),
    addPersonBtn: document.getElementById('add_person_btn'),
    countersEl: document.getElementsByClassName('counters')[0],
}

// for creating dom elements
let parser = new DOMParser();

// selection mode on/off
let selecting = false;


//load initial page
//loadFolder(window.location.href);
demo();
populateCounters();
function demo() {
    UI.staffEl.innerHTML = "Loading Data...";
    fetch(`${window.location.href}api/persons`, {
        headers: {
            'X-CSRF-TOKEN': token
        }
    })
        .then(response => response.json())
        .then(json => {
            if (!json.error) {
                model.staff = json.data;
                renderStaff(json.data);
                loadProjectList();
            }
        })
        .catch(error => {
            console.log(error);
            UI.staffEl.innerHTML = "There was an error loading staff, try again later.";
        });
}
function demoProjects(personId) {
    UI.personProjectsEl.innerHTML = "Loading Data...";
    fetch(`${window.location.href}api/assignments/person/${personId}`, {
        headers: {
            'X-CSRF-TOKEN': token
        }
    })
        .then(response => response.json())
        .then(json => {
            UI.editAssignmentsBtn.dataset['personId'] = personId;
            renderProjects(json.data);
        })
        .catch(error => {
            console.log(error);
            UI.personProjectsEl.innerHTML = "There was an error loading data, try again later.";
        });
}

function loadProjectList() {
    fetch(`${window.location.href}api/projects`, {
        headers: {
            'X-CSRF-TOKEN': token
        }
    })
        .then(response => response.json())
        .then(json => {
            if (json.error) {
                console.log(json.error);
                return;
            }
            model.projects = json.data;
        })
        .catch(error => {
            console.log(error);
        });
}

function populateCounters() {
    let counters = ['projects', 'departments', 'persons'];
    counters.forEach(counter => {
        fetch(`${window.location.href}api/count/${counter}`, {
            headers: {
                'X-CSRF-TOKEN': token
            }
        })
            .then(res => res.json())
            .then(json => {
                if (json.error) {
                    console.log(json.error);
                    return;
                }
                UI.countersEl.getElementsByClassName(counter + '_count')[0]
                    .getElementsByTagName('span')[0].innerHTML = json.data.count;
            })
            .catch(error => console.log("Problem loading counter: " + counter, error))
    });
}

function renderStaff(json) {

    let result = `<table>
        <thead><tr class="staffTableHeader"><th>ID</th><th>Name</th><th>Last Name</th><th>Department</th></tr></thead>
        <tbody>`;
    json
        .map(person => `<tr data-id="${person.id}">
            <td>${person.id}</td>
            <td>${person.name}</td>
            <td>${person.lastname}</td>
            <td>${person.title == null ? '' : person.title}</td>
            <td><a class="delete_link" href="api/persons/delete/${person.id}">
                <img src="${window.location.href}public/img/del.svg" alt="Delete person"></a></td>
            </tr>`)
        .forEach(el => {
            result += el;
        });
    result += `</tbody></table>`;
    UI.staffEl.innerHTML = result;
}

function renderProjects(active) {
    let result = '';
    /**
     * @type {[]}
     */
    let assignedProjects = active.map(project => project['project_id']);
    model.projects
        .map(project => `
            <div data-id="${project.id}" ${assignedProjects.includes(project.id) ? 'data-from-server="true"' : ''}
            class="${assignedProjects.includes(project.id) ? 'assigned' : 'hidden'}">
            ${project.title}
            </div>`
        )
        .forEach(el => {
            result += el;
        });
    UI.personProjectsEl.innerHTML = result;
}

function openStaffModal(id) {
    let person = getPerson(id);
    //console.log(person, id);
    if (person === undefined) {
        alert("Something smells bad...");
        return;
    }
    // show modal
    show(UI.modalEl);
    // lock main tag overflow
    lockMain(true);
    // take persons information from loaded model
    renderPersonInfo(person);
    //populate projects table this person works with
    demoProjects(id);
    // show person info
    show(UI.editPersonFormEl.parentElement);
}

function getPerson(id) {
    return model.staff.find(person => person.id === id);
}

function loadDepartments(el, current, update = false) {
    if (model.departments != null && !update) {
        updateDepartmentSelector(el, model.departments, current);
        return;
    }
    fetch(`${window.location.href}api/departments`, {
        headers: {
            'X-CSRF-TOKEN': token
        }
    })
        .then(response => response.json())
        .then(json => {
            model.departments = json.data;
            updateDepartmentSelector(el, json.data, current);
        })
        .catch(error => {
            console.log(error);
            updateDepartmentSelector(el, { error: "There was an error loading data, try again later." }, current)
        });
}

function updateDepartmentSelector(el, json, current) {
    if (json.error) { console.log(json.error); return; }
    let result = `<option value="" ${current === null ? 'selected' : ''}></option>`;
    json.map(dep => {
        return `<option value="${dep.id}" ${dep.id == current ? 'selected' : ''}>${dep.title}</option>`;
    }).forEach(option => result += option);
    el.innerHTML = result;
}

function renderPersonInfo(person) {
    let department_id = person.department_id == null ? '' : person.department_id;
    let department = person.title == null ? '' : person.title;
    UI.editPersonFormEl.dataset['id'] = person.id;
    UI.editPersonFormEl.innerHTML = `
        <div>
        <label for="name">Name:</label>
        <input name="name" value="${person.name}">
        </div>
        <div>
        <label for="lastname">Lastname:</label>
        <input name="lastname" value="${person.lastname}">
        </div>
        <div>
        <label for="department_id">Department:</label>
        <select name="department_id" class="department_selector">
            <option value="${department_id}" selected>${department}</option>
            <option disabled>Loading data...</option>
        </select>
        </div>
        <button type="submit">Save Changes</button>
    `;
    loadDepartments(UI.editPersonFormEl.getElementsByClassName('department_selector')[0], department_id);
}

UI.staffEl.addEventListener('click', e => {
    e.preventDefault();
    let target = e.target.closest('a');
    if (!target) target = e.target.closest('tr');
    if (target.classList.contains('delete_link')) {
        fetch(target.href, {
            headers: {
                'X-CSRF-TOKEN': token
            }
        }).then(res => res.json()).then(json => {
            demo();
            populateCounters();
        });
        return;
    } else {
        //target = target.closest('tr');
        if (!target || target.classList.contains('staffTableHeader')) return;
        Array.from(UI.staffEl.getElementsByTagName('tr')).forEach(el => el.classList.remove('selected'));
        target.classList.add('selected');
        openStaffModal(Number.parseInt(target.dataset['id']));
    }
    //console.log(target.dataset['id']);
});

UI.editPersonFormEl.addEventListener('submit', e => {
    e.preventDefault();
    /**
     * @type {HTMLFormElement}
     */
    let form = e.target;
    let id = form.dataset['id'];
    let formData = new FormData(form);

    fetch(`${window.location.href}api/persons/edit/${id}`,
        {
            method: 'POST',
            body: formData,
            headers: {
                'X-CSRF-TOKEN': token
            }
        })
        .then(res => res.json())
        .then(json => {
            if (json.error) {
                console.log(json.error);
                alert(json.error);
                return;
            }
            console.log('Person edited. ', json.data.msg);
            // update staff list
            demo();
            populateCounters();
            closeModal();
        });
    //console.log(formData.entries().next());
});

UI.addPersonFormEl.addEventListener('submit', e => {
    e.preventDefault();
    let formData = new FormData(e.target);
    fetch(`${window.location.href}api/persons/add`,
        {
            method: 'POST',
            body: formData,
            headers: {
                'X-CSRF-TOKEN': token
            }
        })
        .then(res => res.json())
        .then(json => {
            if (json.error) {
                console.log(json.error);
                alert(json.error);
                return;
            }
            console.log('Person added. ', json.data);
            // update staff list localy
            model.staff.push({
                id: json.data.id,
                name: formData.get('name'),
                lastname: formData.get('lastname'),
                title: null,
                department_id: null
            });
            renderStaff(model.staff);
            //demo();
            //update counters
            populateCounters();
            closeModal();
        });
});

UI.modalEl.addEventListener('click', e => {
    //e.preventDefault();
    if (e.target.classList.contains('modal_close_btn')) {
        closeModal();
    }
});

function show(el) {
    if (!el) return;
    el.classList.remove('hidden');
}

function hide(el) {
    if (!el) return;
    el.classList.add('hidden');
}

function closeModal() {
    hide(UI.addPersonFormEl.parentElement);
    hide(UI.editPersonFormEl.parentElement);
    hide(UI.modalEl);
    UI.editAssignmentsBtn.removeAttribute('data-edit');
    UI.editAssignmentsBtn.innerText = 'Edit';
    UI.personProjectsEl.classList.remove('edit_mode');
    UI.personProjectsEl.classList.remove('updating');
    // unlock overflow on main tag
    lockMain(false);
}

function lockMain(lock = true) {
    if (lock) {
        document.getElementsByTagName('main')[0].classList.add('overlay_open');
    } else {
        document.getElementsByTagName('main')[0].classList.remove('overlay_open');
    }
}

UI.addPersonBtn.addEventListener('click', e => {
    e.preventDefault();
    show(UI.modalEl);
    show(UI.addPersonFormEl.parentElement);
    Array.from(UI.addPersonFormEl.getElementsByTagName('input'))
        .forEach(input => input.value = '');
    lockMain(true);
});

UI.editAssignmentsBtn.addEventListener('click', e => {
    e.preventDefault();
    /**
     * @type {HTMLButtonElement}
     */
    let btn = e.target;
    console.log("Editing Person ID:", btn.dataset['personId']);
    Array.from(UI.personProjectsEl.getElementsByClassName('hidden'))
        .forEach(el => el.classList.remove('hidden'));
    if (btn.dataset['edit']) {
        btn.innerText = 'Edit';
        btn.removeAttribute('data-edit');
        UI.personProjectsEl.classList.remove('edit_mode');
        // send to server changes
        let test = Array.from(UI.personProjectsEl.getElementsByTagName('div'));
        let add = test
            // filter new assigned projects (has class assigned and didnt come from server)
            .filter(el => el.classList.contains('assigned') && !el.hasAttribute('data-from-server'))
            // map just project ids
            .map(el => Number(el.dataset['id']));

        let remove = test
            // filter removed projects (came from server and doesnt have assigned class)
            .filter(el => !el.classList.contains('assigned') && el.hasAttribute('data-from-server'))
            // map just project ids
            .map(el => Number(el.dataset['id']));
        updatePersonAssignments(btn.dataset['personId'], { add: add, remove: remove });
        //console.log(`${window.location.href}api/assignments/${btn.dataset['personId']}/update`, assignmentsChange);
    } else {
        btn.innerText = 'Save';
        btn.dataset['edit'] = true;
        UI.personProjectsEl.classList.add('edit_mode');
    }
});

function updatePersonAssignments(person_id, changes) {
    let formData = new FormData();
    changes.add.forEach(id => formData.append('add[]', id));
    changes.remove.forEach(id => formData.append('remove[]', id));
    if (changes.add.length < 1 && changes.remove.length < 1) {
        return;
    }
    UI.personProjectsEl.classList.add('updating');
    fetch(`${window.location.href}api/assignments/person/${person_id}/update`,
        {
            method: 'POST',
            body: formData,
            headers: {
                'X-CSRF-TOKEN': token
            }
        })
        .then(res => res.json())
        .then(json => {
            console.log(json);
            alert(`Assignments updated with results:\n
            Removing Assignments: ${json.data.deletion ? json.data.deletion.msg : 'None'}\n
            Adding Assignments: ${json.data.insertion ? json.data.insertion.msg : 'None'}`);
            updateAssignmentsElement(changes);
            UI.personProjectsEl.classList.remove('updating');
        })
        .catch(error => {
            console.log(error);
            alert('There was a problem with update');
            UI.personProjectsEl.classList.remove('updating');
        });
}

/**
 * 
 * @param {{add:[],remove:[]}} changes 
 */
function updateAssignmentsElement(changes) {
    console.log(changes);
    Array.from(UI.personProjectsEl.getElementsByTagName('div'))
        .forEach(el => {
            if (el.hasAttribute('data-from-server') && changes.remove.some(id => id === Number(el.dataset['id']))) {
                el.removeAttribute('data-from-server');
                el.classList.add('hidden');
            } else if (el.classList.contains('assigned')) {
                el.setAttribute('data-from-server', true);
            } else {
                el.classList.add('hidden');
            }
        });
}

UI.personProjectsEl.addEventListener('click', e => {
    e.preventDefault();
    /**
     * @type {HTMLDivElement}
     */
    let target = e.target.closest('div');
    if (!target) return;
    if (!target.hasAttribute('data-id') || !UI.editAssignmentsBtn.hasAttribute('data-edit')) return;
    console.log("Project ID:", target.dataset['id']);
    target.classList.toggle('assigned');
});
