<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="<?= $_SESSION['_token']; ?>">
    <link rel="stylesheet" type="text/css" media="screen" href="<?= BASE_URL; ?>/public/css/main.css">
    <script defer src="<?= BASE_URL; ?>/public/js/main.js" type="text/javascript"></script>
    <title><?= $title; ?></title>
</head>

<body>
    <main>
        <h1><?= $title; ?></h1>
        <section class="counters">
            <div class="projects_count">Total Projects: <span></span></div>
            <div class="departments_count">Total Departments: <span></span></div>
            <div class="persons_count">Total Staff: <span></span></div>
        </section>
        <section class="buttons_section">
            <button id="add_person_btn">Add Person</button>
        </section>
        <section class="staff_view" id="staff">
            <!-- Node for staff list -->
        </section>
        <div class="overlay hidden" id="modal">
            <section class="add_person hidden">
                <form id="add_person">
                    <div>
                        <label for="name">Name:</label>
                        <input name="name" value="">
                    </div>
                    <div>
                        <label for="lastname">Lastname:</label>
                        <input name="lastname" value="">
                    </div>
                    <button type="submit">Add person</button>
                </form>
            </section>
            <section class="person_info hidden">
                <h2>Edit Person</h2>
                <form id="edit_person">

                </form>
                <h3>Assigned to:
                    <button id="edit_assignments_btn">Edit</button>
                </h3>
                <div id="projects_data" class="grid_table">

                </div>
                <!-- <table>
                    <thead>
                        <th>ID</th>
                        <th>Title</th>
                    </thead>
                    <tbody id="projects_data"></tbody>
                </table> -->
            </section>
            <button class="modal_close_btn">Close</button>
        </div>
    </main>
</body>

</html>