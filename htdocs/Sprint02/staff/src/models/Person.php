<?php
function getPersons($queryArgs, $id = null, $page = null)
{
    $query = "SELECT persons.id, name, lastname, title, department_id
        FROM staff.persons LEFT JOIN staff.departments
        ON department_id = departments.id"
        . ($id == null ? '' : " WHERE persons.id=?")
        . " ORDER BY persons.id"
        . ($page == null ? '' : " LIMIT 5 OFFSET ?");

    $conn = connectDB();
    $result = executeQuery($conn, $query, $queryArgs);
    $conn->close();
    return $result;
}

function deletePerson($id)
{
    $query = "DELETE FROM staff.persons WHERE id=?";
    $conn = connectDB();
    $result = executeQuery($conn, $query, [$id], 'delete');
    $conn->close();
    return $result;
}

function editPerson($id, $name, $lastname, $department_id = null)
{
    $query = "UPDATE staff.persons SET name=?, lastname=?, department_id=? WHERE id=?";
    $conn = connectDB();
    $result = executeQuery($conn, $query, [$name, $lastname, $department_id, $id], 'update');
    $conn->close();
    return $result;
}

function addPerson($name, $lastname, $department_id = null)
{ 
    $query = "INSERT INTO staff.persons (`name`, `lastname`, `department_id`) VALUES (?, ?, ?)";
    $conn = connectDB();
    $result = executeQuery($conn, $query, [$name, $lastname, $department_id], 'insert');
    $conn->close();
    return $result;
}
