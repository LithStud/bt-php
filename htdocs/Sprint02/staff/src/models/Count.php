<?php

function countRows($table)
{
    $query = "SELECT COUNT(*) AS `count` FROM staff.$table";
    $conn = connectDB();
    $result = executeQuery($conn, $query, []);
    $conn->close();
    return $result[0]; // there shouldn never be more than one entry
}
