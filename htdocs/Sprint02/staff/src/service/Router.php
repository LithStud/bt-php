<?php
function handleRoute($path, $httpMethod)
{
    global $routes;

    if ($path == '') {
        $path = '/';
    }

    foreach ($routes[$httpMethod] as $index => $route) {
        $pattern = '/' . str_replace('/', '\/', $route['_pattern']) . '/i';
        if (preg_match($pattern, $path, $matches)) {
            //var_dump($matches[0]);
            //var_dump($path);
            if ($matches[0] == $path) {
                return [$route, $matches];
                break;
            }
        }
    }

    return false;
}
