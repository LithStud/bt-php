<?php

function csrfToken()
{
    if (empty($_SESSION['_token'])) {
        $_SESSION['_token'] = bin2hex(random_bytes(64));
    }
}

function csrfVerify($isProtected)
{
    if ($isProtected) {
        $token = isset($_SERVER['HTTP_X_CSRF_TOKEN']) ? $_SERVER['HTTP_X_CSRF_TOKEN'] : '';
        return hash_equals($_SESSION['_token'], $token);
    } else {
        return true;
    }
}
