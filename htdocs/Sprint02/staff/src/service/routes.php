<?php
$routes['GET'][] = [
    '_name' => 'main',
    '_pattern' => '^(?:/|$)',
    '_controller' => 'MainPage',
    '_action' => 'mainPage',
    '_protected' => false
];

$routes['GET'][] = [
    '_name' => 'persons',
    '_pattern' => '^/api/persons(/\d+)?(/page/\d+)?(?:/|$)', // persons/{ID}/page/{PAGE_NUM}
    '_controller' => 'PersonsController',
    '_action' => 'listPersons',
    '_protected' => false
];

$managePersonRoute = [
    '_name' => 'personsEdit',
    '_pattern' => '^/api/persons/(edit|delete)/(\d+)(?:/|$)', // persons/{edit|delete}/{ID}
    '_controller' => 'PersonsController',
    '_action' => 'managePerson',
    '_protected' => true
];

$routes['GET'][] = $managePersonRoute;
$routes['POST'][] = $managePersonRoute;

$routes['GET'][] = [
    '_name' => 'departments',
    '_pattern' => '^/api/departments(/\d+)?(/page/\d+)?(?:/|$)', // departments/{ID}/page/{PAGE_NUM}
    '_controller' => 'DepartmentsController',
    '_action' => 'listDepartments',
    '_protected' => false
];

$routes['GET'][] = [
    '_name' => 'projects',
    '_pattern' => '^/api/projects(/\d+)?(/page/\d+)?(?:/|$)', // departments/{ID}/page/{PAGE_NUM}
    '_controller' => 'ProjectsController',
    '_action' => 'listProjects',
    '_protected' => false
];

$routes['GET'][] = [
    '_name' => 'assignments',
    '_pattern' => '^/api/assignments(/person/\d+)?(/AND|/OR)?(/project/\d+)?(/page/\d+)?(?:/|$)', // /api/assignments/person/{ID}/{and|or}/project/{ID}/page/{ID}
    '_controller' => 'AssignmentsController',
    '_action' => 'listAssignments',
    '_protected' => false
];

$routes['POST'][] = [
    '_name' => 'assignments',
    '_pattern' => '^/api/assignments/person/(\d+)/update(?:/|$)', // /api/assignments/person/{ID}/{and|or}/project/{ID}/page/{ID}
    '_controller' => 'AssignmentsController',
    '_action' => 'updateAssignments',
    '_protected' => true
];

$routes['POST'][] = [
    '_name' => 'assignments',
    '_pattern' => '^/api/test(?:/|$)', // /api/assignments/person/{ID}/{and|or}/project/{ID}/page/{ID}
    '_controller' => 'Test',
    '_action' => 'test',
    '_protected' => false
];

$routes['POST'][] = [
    '_name' => 'personsAdd',
    '_pattern' => '^/api/persons/add(?:/|$)', // /api/assignments/person/{ID}/{and|or}/project/{ID}/page/{ID}
    '_controller' => 'PersonsController',
    '_action' => 'addPersonControl',
    '_protected' => true
];

$routes['GET'][] = [
    '_name' => 'counter',
    '_pattern' => '^/api/count/(persons|departments|projects)(?:/|$)', // /api/assignments/person/{ID}/{and|or}/project/{ID}/page/{ID}
    '_controller' => 'CountController',
    '_action' => 'countControl',
    '_protected' => false
];
