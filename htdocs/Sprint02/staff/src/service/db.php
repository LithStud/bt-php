<?php
function connectDB()
{
    @$db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_DATABASE);
    if ($db->connect_errno) {
        // error logging
        return false;
    }
    $db->set_charset("utf8");
    return $db;
}

function executeQuery(&$conn, $query, $args = [], $mode = 'r')
{
    $argsCount = substr_count($query, '?');
    if ($argsCount !== count($args)) {
        return array("error" => "Mismatched count of args and query.");
    }

    $prep = $conn->prepare($query);
    // build args
    if (!empty($args)) {
        $types = '';
        foreach ($args as $arg) {
            if (is_int($arg)) {
                $types .= 'i';
            } else {
                $types .= 's';
            }
        }

        if (!$prep->bind_param($types, ...$args)) {
            return array("error" => "Failed to bind params.");
        }
    }

    if ($exec = $prep->execute()) {
        switch ($mode) {
            case 'r': // default queary that returns a selection of rows
                $result = $prep->get_result();
                return $result->fetch_all(MYSQLI_ASSOC);
                break;

            default: // insertion, deletion, updating
                if ($prep->affected_rows > 0) {
                    $response = ["msg" => "Operation was succesfull"];
                    if ($mode == 'insert') $response["id"] = $prep->insert_id;
                    return $response;
                } else return ["error" => "No rows in database has been changed"];
                break;
        }
    } else return ["error" => "There was an error trying to execute query"];
}
