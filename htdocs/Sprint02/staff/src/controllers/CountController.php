<?php
require_once (ROOT_DIR . '/src/models/Count.php');
function countControl($args)
{
    switch (strtolower($args[1])) {
        case 'persons':
            return renderJsonResponse(countRows('persons'));
            break;
        case 'departments':
            return renderJsonResponse(countRows('departments'));
            break;
        case 'projects':
            return renderJsonResponse(countRows('projects'));
            break;

        default:
            return renderJsonResponse(["error" => "bad endpoint"]);
            break;
    }
}
