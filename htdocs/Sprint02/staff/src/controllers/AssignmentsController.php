<?php

function listAssignments($args = [])
{
    $person_id = null;
    $operator = null;
    $project_id = null;
    $page = null;

    $queryArgs = [];
    if (count($args) > 1) {
        if (isset($args[2]) && $args[2]) { // "/and" or "/or"
            $operator = strtolower(trim($args[2], '/'));
            if ($operator != 'and' && $operator != 'or')
                $operator = null;
        }

        // if both found "/person/2" "/project/2" but no correct operator
        if ((isset($args[1]) && $args[1]) && (isset($args[3]) && $args[3]) && $operator == null) {
            return renderJsonResponse(["error" => "Incorrect query, missing AND or OR operator when submiting both Person ID and Project ID"]);
        }

        if (isset($args[1]) && $args[1]) { // "/person/2"
            $person_id = intval(explode('/', trim($args[1], '/'))[1]);
            $queryArgs[] = $person_id;
        }

        if (isset($args[3]) && $args[3]) { // "/project/2"
            $project_id = intval(explode('/', trim($args[3], '/'))[1]);
            $queryArgs[] = $project_id;
        }

        if (isset($args[4]) && $args[4]) { // "/page/3"
            $page = intval(explode('/', trim($args[4], '/'))[1]);
            $queryArgs[] = (5 * ($page - 1));
        }
    }

    $query = "SELECT person_id, persons.name, persons.lastname, projects.title, project_id
        FROM staff.persons_projects
        LEFT JOIN staff.persons ON person_id = persons.id
        RIGHT JOIN staff.projects ON project_id = projects.id"
        . ($person_id != null || $project_id != null ? " WHERE" : '')
        . ($person_id == null ? '' : " staff.persons_projects.person_id=?")
        . ($operator == null ? '' : " $operator")
        . ($project_id == null ? '' : " staff.persons_projects.project_id=?")
        . " ORDER BY project_id"
        . ($page == null ? '' : " LIMIT 5 OFFSET ?");

    $conn = connectDB();
    $result = executeQuery($conn, $query, $queryArgs);
    $conn->close();
    return renderJsonResponse($result);
}

function updateAssignments($args = [])
{
    $person_id = isset($args[1]) && intval($args[1]) ? $args[1] : null;
    if ($person_id === null) {
        return renderJsonResponse(['error' => 'No person id supplied']);
    }

    $result['updating'] = 'Updating results';
    $conn = connectDB();
    if (isset($_POST['remove'])) {
        $removedIds = [$person_id];
        foreach ($_POST['remove'] as $value) {
            $removedIds[] = intval($value);
        }
        if (count($removedIds) > 0) {
            $query = "DELETE FROM staff.persons_projects WHERE persons_projects.person_id=? AND"
                . " persons_projects.project_id IN (" . trim(str_repeat(',?', count($removedIds) - 1), ',') . ")";
            $result['deletion'] = executeQuery($conn, $query, $removedIds, 'delete');
        }
    }

    if (isset($_POST['add'])) {
        $addedIds = [];
        $values = null;
        foreach ($_POST['add'] as $value) {
            $addedIds[] = $person_id;
            $addedIds[] = intval($value);
            $values .= "(?,?),";
        }
        if (count($addedIds) > 0) {
            $query = "INSERT INTO staff.persons_projects (person_id, project_id)"
                . " VALUES " . trim($values, ',');
            $result['insertion'] = executeQuery($conn, $query, $addedIds, 'insert');
        }
    }

    $conn->close();
    return renderJsonResponse($result);
}
