<?php

function listProjects($args = [])
{
    $id = null;
    $page = null;

    $queryArgs = [];
    if (count($args) > 1) {
        if (isset($args[1]) && $args[1]) { // "/2"
            $id = intval(trim($args[1], '/'));
            $queryArgs[] = $id;
        }

        if (isset($args[2]) && $args[2]) { // "/page/3"
            $page = intval(explode('/', trim($args[2], '/'))[1]);
            $queryArgs[] = (5 * ($page - 1));
        }
    }

    $query = "SELECT id, title, description FROM staff.projects"
        . ($id == null ? '' : " WHERE id=?")
        . " ORDER BY id"
        . ($page == null ? '' : " LIMIT 5 OFFSET ?");

    $conn = connectDB();
    $result = executeQuery($conn, $query, $queryArgs);
    $conn->close();
    return renderJsonResponse($result);
}
