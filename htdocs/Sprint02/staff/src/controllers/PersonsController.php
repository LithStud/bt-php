<?php
require_once sanitizeSlash(ROOT_DIR . '/src/models/Person.php');

function listPersons($args = [])
{
    $id = null;
    $page = null;
    $queryArgs = [];

    if (count($args) > 1) {
        if (isset($args[1]) && $args[1]) { // "/2"
            $id = intval(trim($args[1], '/'));
            $queryArgs[] = $id;
        }

        if (isset($args[2]) && $args[2]) { // "/page/3"
            $page = intval(explode('/', trim($args[2], '/'))[1]);
            $queryArgs[] = (5 * ($page - 1));
        }
    }

    $result = getPersons($queryArgs, $id, $page);
    return renderJsonResponse($result);
}

function managePerson($args = [])
{
    $id = isset($args[2]) && is_numeric($args[2]) ? intval($args[2]) : null;
    if ($id != null && $id > 0 && isset($args[1]) && $args[1]) {
        switch (strtolower($args[1])) {
            case 'edit': // we want to edit person record
                $name = $_POST['name'];
                $lastname = $_POST['lastname'];
                $department_id = $_POST['department_id'] ? $_POST['department_id'] : null;
                $result = editPerson($id, $name, $lastname, $department_id);
                return renderJsonResponse($result);
                //return renderJsonResponse(["msg"=>"Editing to be implemented."]);
                break;

            case 'delete': // we want to delete this persons
                $result = deletePerson($id);
                return renderJsonResponse($result);
                break;
        }
    } else {
        return renderJsonResponse(["error" => "parameters missing"]);
    }
}

function addPersonControl($args = [])
{ 
    $name = $_POST['name'];
    $lastname = $_POST['lastname'];
    $department_id = isset($_POST['department_id']) && is_numeric($_POST['department_id']) ? intval($_POST['department_id']) : null;
    if (empty($name) || empty($lastname)) {
        return renderJsonResponse(["error" => "parameters missing"]);
    }
    return renderJsonResponse(addPerson($name, $lastname, $department_id));
}
