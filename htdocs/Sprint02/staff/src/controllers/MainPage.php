<?php

function mainPage()
{
    $args = array("title" => "Staff Management System");
    return renderResponse(sanitizeSlash(ROOT_DIR . '/src/templates/indexHTML.php'), $args);
}
