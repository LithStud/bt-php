<?php
function renderResponse($template, $args)
{
    ob_start();
    extract($args);
    require($template);
    return ob_get_clean();
}

/**
 * Creates JSON response from suplied $obj
 */
function renderJsonResponse($obj)
{
    header("Content-Type: application/json; charset=UTF-8");
    if (!array_key_exists('error', $obj)) {
        $obj = ["data"=>$obj];
    }
    return json_encode($obj, JSON_UNESCAPED_UNICODE);
}
