<?php
/**
 * URL config
 */
define('ROOT_DIR', $rootDirectory);
define('BASE_URL', $httpRoot . $httpDir);
define('HTTP_PATH', $path);

/**
 * Database config
 */
define('DB_HOST', 'localhost');
define('DB_USER', 'staff');
define('DB_PASS', 'staffpassword');
define('DB_DATABASE', 'staff');

define('DB_SELECT_LIMIT', 5);
