-- names
SET NAMES utf8;

-- schema
DROP SCHEMA IF EXISTS `staff`;
CREATE SCHEMA IF NOT EXISTS `staff` DEFAULT CHARACTER SET utf8 COLLATE utf8_lithuanian_ci;

-- staff
USE `staff`;

SET FOREIGN_KEY_CHECKS = 0;

-- staff.departments
  DROP TABLE IF EXISTS `departments`;
CREATE TABLE IF NOT EXISTS `departments` (
    `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(128) COLLATE utf8_lithuanian_ci NOT NULL DEFAULT '""',
    PRIMARY KEY (`id`),
    UNIQUE KEY `title_UNIQUE` (`title`)
  ) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8 COLLATE = utf8_lithuanian_ci;

-- staff.persons
  DROP TABLE IF EXISTS `persons`;
CREATE TABLE IF NOT EXISTS `persons` (
    `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(128) COLLATE utf8_lithuanian_ci NOT NULL DEFAULT '""',
    `lastname` VARCHAR(128) COLLATE utf8_lithuanian_ci NOT NULL DEFAULT '""',
    `department_id` bigint(20) UNSIGNED DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `name_lastname_UNIQUE` (`name`, `lastname`),
    KEY `FK_persons_departments` (`department_id`),
    INDEX `person_name_idx` (`name`),
    INDEX `person_lastname_idx` (`lastname`),
    CONSTRAINT `FK_persons_departments` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE
    SET NULL ON UPDATE CASCADE
  ) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8 COLLATE = utf8_lithuanian_ci;

-- staff.projects
  DROP TABLE IF EXISTS `projects`;
CREATE TABLE IF NOT EXISTS `projects` (
    `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(128) COLLATE utf8_lithuanian_ci NOT NULL DEFAULT '""',
    `description` text COLLATE utf8_lithuanian_ci NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `title_UNIQUE` (`title`)
  ) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8 COLLATE = utf8_lithuanian_ci;
  
-- staff.persons_projects
  DROP TABLE IF EXISTS `persons_projects`;
CREATE TABLE IF NOT EXISTS `persons_projects` (
    `person_id` bigint(20) UNSIGNED NOT NULL,
    `project_id` bigint(20) UNSIGNED NOT NULL,
    PRIMARY KEY (`person_id`, `project_id`),
    INDEX `person_id_idx` (`person_id`),
    INDEX `project_id_idx` (`project_id`)
  ) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8 COLLATE = utf8_lithuanian_ci;


-- triggers
-- after delete in persons
DROP TRIGGER IF EXISTS `delete_p_persons_projects`;
DELIMITER //
CREATE TRIGGER IF NOT EXISTS `delete_p_persons_projects`
AFTER DELETE ON `persons`
FOR EACH ROW
BEGIN
  DELETE FROM `persons_projects` WHERE persons_projects.person_id = OLD.id;
END; //
DELIMITER ;
-- after delete in projects
DROP TRIGGER IF EXISTS `delete_proj_persons_projects`;
DELIMITER //
CREATE TRIGGER IF NOT EXISTS `delete_proj_persons_projects`
AFTER DELETE ON `projects`
FOR EACH ROW
BEGIN
  DELETE FROM `persons_projects` WHERE persons_projects.project_id = OLD.id;
END; //
DELIMITER ;

  SET FOREIGN_KEY_CHECKS = 1;