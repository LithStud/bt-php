<?php
/**
 * Begin Session
 */
session_start();

/**
 * Lets make OS independant slashes
 */
function sanitizeSlash($url)
{
    $url = str_replace("\\", DIRECTORY_SEPARATOR, $url);
    return str_replace("/", DIRECTORY_SEPARATOR, $url);
}

// Set protocol
$httpProtocol = 'http://';

if (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on') {
    $httpProtocol = 'https://';
}

// Set http root
$httpRoot = $httpProtocol . $_SERVER['HTTP_HOST'];

// Set http directory
$httpDir = dirname($_SERVER['SCRIPT_NAME']);

if ($httpDir == DIRECTORY_SEPARATOR) {
    $httpDir = '';
}

// Set path
$path = '';

if (isset($_SERVER['REQUEST_URI'])) {
    $path = urldecode(substr($_SERVER['REQUEST_URI'], strlen($httpDir)));
}

$path = parse_url($path, PHP_URL_PATH);

if ($path == '/') {
    $path = '';
}

$httpMethod = $_SERVER['REQUEST_METHOD'];

$rootDirectory = __DIR__;

// Forcefully filter $_GET and $_POST
filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
filter_input_array(INPUT_GET);

/**
 * Loading required API parts
 */
// Load config (custom constants)
require_once sanitizeSlash(__DIR__ . '/config/config.php');
// Templating functions
require_once sanitizeSlash(__DIR__ . '/src/helpers/templating.php');
// services
require_once sanitizeSlash(__DIR__ . '/src/service/db.php');
require_once sanitizeSlash(__DIR__ . '/src/service/routes.php');
require_once sanitizeSlash(__DIR__ . '/src/service/Router.php');
require_once sanitizeSlash(__DIR__ . '/src/service/security.php');

// handle security
csrfToken();

if ($route = handleRoute($path, $httpMethod)) {
    // dont check csrf token for index.php load
    if ($route[0]['_name'] !== 'main' && !csrfVerify($route[0]['_protected'])) {
        echo renderJsonResponse(["error" => "403"]);
        exit();
    }

    include sanitizeSlash(__DIR__ . '/src/controllers/' . $route[0]['_controller'] . '.php');
    if (function_exists($route[0]['_action'])) {
        //$result = $route[0]['_action']($route[1]);
        echo $route[0]['_action']($route[1]);
    }
} else {
    //header('');
    echo renderJsonResponse(["error" => "404"]);
    exit();
}
