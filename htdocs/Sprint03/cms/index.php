<?php
require __DIR__ . '/vendor/autoload.php';
session_start();
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use DI\Container;

use App\Services\Router;

define('BASE_DIR', dirname(__FILE__));

$request = Request::createFromGlobals();

$path   = $request->getPathInfo();
$method = $request->getMethod();

$router = new Router();
$container = new Container();

// to save space in index.php route registering moved out into routes.php
//include 'routes.php';
$router->registerRoute(Config::METHOD_GET, '^(?:/|$)', 'App\Controllers\IndexController', 'index');
$router->registerRoute(Config::METHOD_GET, '^/([0-9]+)(?:/|$)', 'App\Controllers\IndexController', 'index');
// login page
$router->registerRoute(Config::METHOD_GET, '^/login(?:/|$)', 'App\Controllers\AuthController', 'redirectToLogin');
// POST used for sending login credentials
$router->registerRoute(Config::METHOD_POST, '^/login(?:/|$)', 'App\Controllers\AuthController', 'login');
// logout
$router->registerRoute(Config::METHOD_GET, '^/logout(?:/|$)', 'App\Controllers\AuthController', 'logout');

$router->registerRoute(Config::METHOD_POST, '^/add(?:/|$)', 'App\Controllers\CmsController', 'addPost', true);


define ('BASE_URL', $request->getBaseUrl());

$auth = $container->get('App\Controllers\AuthController');
$auth::csrfToken();

if ($match = $router->match($path, $method)) {
    if ($method == 'POST' && !$auth::verifyCsrf()) {
        $router->redirectTo(BASE_URL);
    }
    $controller = $container->get($match['route']['_controller']);
    $action = $match['route']['_action'];
    $response = $controller->$action($match['args']);
} else {
    $response = "404";
    // redirect to 404
}

echo $response;
/* $query = "SELECT title, post FROM posts";
$o = new Db();
$o->setQuery($query);
$post = $o->executeQuery()->get();
var_dump($post); */

/* $query = "INSERT INTO posts (title, post) VALUES ('Some Title', 'Some random post, that might be rather long....')";
$o->setMode('insert');
$o->setNeedLastId(true);
$o->setQuery($query);
$post = $o->executeQuery()->get();
var_dump($post); */

/* $query = "SELECT title, post FROM posts";
$o->setMode();
$o->setQuery($query);
$post = $o->executeQuery()->get();
var_dump($post); */