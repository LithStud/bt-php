<?php
class Config
{ 
    const DB_DRIVER = 'mysql';
    const DB_HOST = 'localhost';
    const DB_PORT = '3306';
    const DB_USER = 'cms';
    const DB_PASS = 'cmsuser';
    const DB_DATABASE = 'cms';

    const METHOD_POST = 'POST';
    const METHOD_GET = 'GET';
}
