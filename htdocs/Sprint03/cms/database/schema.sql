-- names
SET NAMES utf8;

-- schema
DROP SCHEMA IF EXISTS `cms`;
CREATE SCHEMA IF NOT EXISTS `cms` DEFAULT CHARACTER SET utf8 COLLATE utf8_lithuanian_ci;

-- cms
USE `cms`;

SET FOREIGN_KEY_CHECKS = 0;

-- cms.posts
DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
    `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(128) COLLATE utf8_lithuanian_ci NOT NULL DEFAULT '""',
    `post` text COLLATE utf8_lithuanian_ci NOT NULL,
    `user_id` bigint(20) UNSIGNED DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `title_UNIQUE` (`title`),
    CONSTRAINT `FK_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE
  ) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8 COLLATE = utf8_lithuanian_ci;

-- cms.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
    `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` varchar(128) COLLATE utf8_lithuanian_ci NOT NULL,
    `email` VARCHAR(128) COLLATE utf8_lithuanian_ci NOT NULL,
    `password` VARCHAR(255) COLLATE utf8_lithuanian_ci NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `email_UNIQUE` (`email`)
  ) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8 COLLATE = utf8_lithuanian_ci;

SET FOREIGN_KEY_CHECKS = 1;