-- utf8
SET NAMES utf8;

-- Turn off foreign key check
SET FOREIGN_KEY_CHECKS=0;

-- Seeding table users
-- admin pass: admin
-- user pass: user
DELETE FROM cms.users;
ALTER TABLE cms.users AUTO_INCREMENT=1;
INSERT INTO cms.users (`name`, `email`, `password`) VALUES
('admin', 'admin@net.no', '$2y$10$ZChPXjEcET2A4Dq.C0ahbOgBOBUhi4UcJO6O5l8KsRkqcJm5Vycli'),
('user', 'simpleton@gmail.com', '$2y$10$0PlnhMfwl/l8EP4BXBnwTe/pkoo7q8VIFtM5faIc1Alczl0jxFIoi');

-- Turn on foreign key check
SET FOREIGN_KEY_CHECKS=1;

