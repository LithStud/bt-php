<div class="row">
    <?php
    if ($user) echo 'Welcome ' . $user['name'];
    foreach ($posts as $value) : ?>
        <div class="col-12">
            <article class="card border-secondary mb-3">
                <h5 class="card-header"><?= $value['title']; ?></h5>
                <div class="card-body">
                    <p class="card-text"><?= $value['post']; ?></p>
                </div>
                <div class="card-footer">Posted by: <?= $value['name'] == null ? 'System' : $value['name']; ?></div>
            </article>
        </div>
    <?php endforeach; ?>
</div>
<?php if ($totalPages > 1) : ?>
    <nav aria-label="Blog pages">
        <ul class="pagination justify-content-center">
            <?php if ($page > 1) : ?>
                <li class="page-item"><a class="page-link" href="<?= ($page - 1); ?>">Previous</a></li>
            <?php endif; ?>
            <li class="page-item disabled"><a class="page-link" href="#"><?= $page; ?></a></li>
            <?php if ($page < $totalPages) : ?>
                <li class="page-item"><a class="page-link" href="<?= ($page + 1); ?>">Next</a></li>
            <?php endif; ?>
        </ul>
    </nav>
<?php endif; ?>