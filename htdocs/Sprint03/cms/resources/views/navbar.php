<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Blog</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <?php if ($user) : ?>
                <li class="nav-item">
                    <a class="nav-link" href="#" data-toggle="modal" data-target="#createPostModal">Create Post</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Something</a>
                </li>
            <?php endif; ?>
        </ul>
        <?php if ($user) : ?>
            <a class="nav-item btn btn-primary" href="logout">
                Logout
            </a>
        <?php else : ?>
            <button type="button" class="nav-item btn btn-primary" data-toggle="modal" data-target="#loginModal">
                Login
            </button>
        <?php endif; ?>
    </div>
</nav>

<?php if (!$user) : ?>
    <!-- Login Modal -->
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="loginModalLabel">Login</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="login" method="POST">
                        <input type="hidden" name='_token' value="<?= $_SESSION['_token']; ?>">
                        <div class="form-group">
                            <label for="username">Username:</label>
                            <input type="text" class="form-control" name="user" id="username" aria-describedby="usernameHelp" placeholder="Username">
                        </div>
                        <div class="form-group">
                            <label for="user_password">Password:</label>
                            <input type="password" class="form-control" name="password" id="user_password" placeholder="Password">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php else : ?>
    <!-- Logged in user so add Post creation Modal -->
    <div class="modal fade" id="createPostModal" tabindex="-1" role="dialog" aria-labelledby="createPostModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createPostModalLabel">Login</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="add" method="POST">
                        <input type="hidden" name='_token' value="<?= $_SESSION['_token']; ?>">
                        <input type="hidden" name='user_id' value="<?= $user['id']; ?>">
                        <div class="form-group">
                            <label for="title">Title:</label>
                            <input type="text" class="form-control" name="title" id="title" aria-describedby="titleHelp" placeholder="Title">
                        </div>
                        <div class="form-group">
                            <label for="post">Body:</label>
                            <textarea class="form-control" name="post" id="post"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>