<?php
namespace App\Interfaces;

interface DbInterface {
    public function get();
    public function executeQuery();
    public function setQuery($query);
    public function setMode($mode = 'r');
    public function setNeedLastId($need = false);
}