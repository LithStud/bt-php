<?php
namespace App\Interfaces;

interface ModelInterface
{
	public function getTable();

	public function GetFillable();
}