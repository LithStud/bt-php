<?php
namespace App\Controllers;

use App\Controllers\BaseController;
use App\Services\Helpers;

class IndexController extends BaseController
{
    public function index($args = [])
    {
        $orm = $this->container->get('App\Services\Orm');

        $view = $this->container->get('App\Services\View');

        $page = count($args) > 1 ? $args[1] : 1;

        $offset = (5 * (intval($page) - 1));

        $query = "SELECT title, post, users.name AS name FROM posts LEFT JOIN users ON posts.user_id = users.id ORDER BY posts.id DESC LIMIT 5 OFFSET ?";
        $posts = $orm->select($query, [$offset]);
        $query = "SELECT COUNT(id) as total_posts FROM posts";
        $totalPosts = intval($orm->select($query)[0]['total_posts']);
        $totalPages = floor($totalPosts / 5);

        /*
        $query = "SELECT title, post FROM posts WHERE id=?";
        $args = [1];
        $post = $orm->select($query, $args);
        var_dump($post);

        $postModel = $this->container->get('App\Models\Post');
        $orm->setModel($postModel);
        $result = $orm->insert(["Title 07", "Posting of title 01"]);
        var_dump($result);

        $query = "SELECT title, post FROM posts";
        $post = $orm->select($query);
        var_dump($post); */
        //$orm = $this->container->get('App\Services\Orm');
        //$result = $orm->select("SELECT * FROM cms.posts");
        $user = (isset($_SESSION['auth']) && $_SESSION['auth']) ? $_SESSION['user'] : false;
        $navbar = $view::render('navbar', compact('user'));
        $content = $view::render('posts', compact(['posts', 'page', 'totalPages', 'user']));
        return $view::render('index', compact(['navbar', 'content']));
    }
}
