<?php
namespace App\Controllers;

use App\Controllers\BaseController;

class CmsController extends BaseController
{
    public function addPost()
    {
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $router = $this->container->get('App\Services\Router');

        if (isset($_POST['title'])) {
            $title = $_POST['title'];
        }

        if (isset($_POST['post'])) {
            $post = $_POST['post'];
        }

        $user_id = null;
        if (isset($_POST['user_id'])) {
            $user_id = $_POST['user_id'];
        }

        if (empty($title) || empty($post)) {
            $router->redirectTo(BASE_URL);
        }

        $orm = $this->container->get('App\Services\Orm');
        //$view = $this->container->get('App\Services\View');

        $postModel = $this->container->get('App\Models\Post');
        $orm->setModel($postModel);
        $result = $orm->insert([$title, $post, $user_id]);

        
        $router->redirectTo(BASE_URL);
    }
}
