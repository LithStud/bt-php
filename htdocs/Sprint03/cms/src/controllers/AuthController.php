<?php
namespace App\Controllers;

use App\Controllers\BaseController;
use App\Services\Helpers;

class AuthController extends BaseController
{
    public function csrfToken()
    {
        if (empty($_SESSION['_token'])) {
            $_SESSION['_token'] = bin2hex(random_bytes(32));
        }
    }

    public function verifyCsrf()
    {
        if (hash_equals($_SESSION['_token'], $_POST['_token'])) {
            return true;
        }
        return false;
    }

    public function redirectToLogin()
    {
        $router = $this->container->get('App\Services\Router');
        $router->redirectTo(BASE_URL);
    }
    public function logout()
    {
        $_SESSION['auth'] = false;
        //$_SESSION['user']['role']  = $user[0]['role'];
        $_SESSION['user']['id']    = null;
        $_SESSION['user']['name'] = null;
        $router = $this->container->get('App\Services\Router');
        $router->redirectTo(BASE_URL);
    }
    public function login()
    {
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $username = '';
        $pass  = '';
        $hash = null;

        if (isset($_POST['user'])) {
            $username = $_POST['user'];
        }

        if (isset($_POST['password'])) {
            $pass = $_POST['password'];
        }

        $orm = $this->container->get('App\Services\Orm');
        $query = "SELECT users.id, users.name, users.password FROM cms.users WHERE users.name = ?";
        $user = $orm->select($query, [$username]);
        $router = $this->container->get('App\Services\Router');

        if (!empty($user)) {
            $hash = $user[0]['password'];
        }

        if ($hash and password_verify($pass, $hash)) {
            $_SESSION['auth'] = true;
            //$_SESSION['user']['role']  = $user[0]['role'];
            $_SESSION['user']['id']    = $user[0]['id'];
            $_SESSION['user']['name'] = $username;
            //$role = 'user';
            /* if ($user[0]['role'] === '1') {
                $role = 'admin';
            } */
            //$_SESSION['messages']['success'][] = __('Welcome') . ', ' . $role;
            return $router->redirectTo(BASE_URL);
        } else {
            return $router->redirectTo(BASE_URL);
            /* $_SESSION['auth']          = false;
            $_SESSION['user']['role']  = null;
            $_SESSION['user']['id']    = null;
            $_SESSION['user']['email'] = null;
            $_SESSION['messages']['warning'][] = __('We don\'t have any data about this user');
            return redirect(BASE_URL . '/login'); */
        }
    }
}
