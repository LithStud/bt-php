<?php
namespace App\Models;

use App\Models\Model;

class Post extends Model
{
    protected $fillable = ['title', 'post', 'user_id'];
    protected $table = 'posts';
}
