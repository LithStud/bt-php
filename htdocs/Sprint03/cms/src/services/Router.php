<?php
namespace App\Services;

class Router
{
    private $routes = [];

    public function registerRoute($method, $pattern, $controller, $action, $isProtected = false)
    {
        /*
        '_name' => 'index',
        '_pattern' => '^(?:/|$)',
        '_controller' => 'App\Controllers\IndexController',
        '_action' => 'index',
        '_protected' => false
        */
        $this->routes[$method][] = [
            // '_name' => 'index',
            '_pattern' => $pattern,
            '_controller' => $controller,
            '_action' => $action,
            '_protected' => $isProtected
        ];
    }

    public function match($path, $method)
    {
        foreach ($this->routes[$method] as $route) {
            $pattern = '/' . str_replace('/', '\/', $route['_pattern']) . '/i';
            if (preg_match($pattern, $path, $matches)) {
                if ($matches[0] == $path) {
                    return ['route' => $route, 'args' => $matches];
                    break;
                }
            }
        }
    }

    public function showRegisteredRoutes()
    {
        return $this->routes;
    }

    public static function redirectTo($url, $status = 302, $replace = true)
    {
        header('Location: ' . $url, $replace, $status);
        die();
    }
}
