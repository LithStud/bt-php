<?php
/* require 'config/Config.php';
require 'DbInterface.php'; */

namespace App\Services;

use App\Interfaces\DbInterface;
use Config;

class Db implements DbInterface
{
    protected $db = false;
    protected $query;
    //protected $types = [];
    protected $args = [];
    protected $needLastId = false;
    protected $mode = 'r'; // default is reading

    protected $result;

    private $currentDb;
    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    private function connect()
    {
        $dsn = $this->config::DB_DRIVER . ":"
            . "host=" . $this->config::DB_HOST . ";"
            . "port=" . $this->config::DB_PORT . ";"
            . "dbname=" . $this->config::DB_DATABASE;
        try {
            @$db = new \PDO($dsn, $this->config::DB_USER, $this->config::DB_PASS, array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', \PDO::ATTR_ERRMODE => \PDO::ERRMODE_SILENT));
        } catch (\PDOException $e) {
            $this->db = false;
        }
        $this->db = $db;
        $this->currentDb = $this->config::DB_DATABASE;
    }

    public function setQuery($query, $args = [])
    {
        $this->query = $query;
        // make sure its number based indexes without skips
        $this->args = array_values($args);
    }

    public function setMode($mode = 'r')
    {
        $this->mode = $mode;
    }

    public function setNeedLastId($need = false)
    {
        $this->needLastId = $need;
    }

    public function getCurrentDb()
    {
        if ($this->currentDb) {
            return $this->currentDb;
        }
        return $this->config::DB_DATABASE;
    }

    public function executeQuery()
    {
        $argsCount = substr_count($this->query, '?');
        if ($argsCount !== count($this->args)) {
            $this->result = ["error" => "Mismatched count of args in query."];
            return $this;
        }

        $this->connect();

        if (!$this->db) {
            $this->result = ["error" => "Error connecting to DB"];
            return $this;
        }

        $run = $this->db->prepare($this->query);

        if (!$run) {
            $this->result = ["error" => "Error preparing Query"];
            return $this;
        }

        // build args
        if (!empty($this->args)) {
            foreach ($this->args as $key => $arg) {
                switch (gettype($arg)) {
                    case 'integer':
                        $param = \PDO::PARAM_INT;
                        break;
                    case 'string':
                        $param = \PDO::PARAM_STR;
                        break;
                    case 'NULL':
                        $param = \PDO::PARAM_NULL;
                        break;
                    case 'boolean':
                        $param = \PDO::PARAM_BOOL;
                        break;
                    default:
                        $param = false;
                        break;
                }
                if ($param && !$run->bindValue($key + 1, $arg, $param)) {
                    $this->result = ["error" => "Failed to bind params."];
                    //$this->args = []; // remove arg list
                    return $this;
                }
            }

            // empty args list after binding complete;
            //$this->args = [];
        }

        $exec = $run->execute();

        if (!$exec) {
            $this->result = ["error" => "Query execution failed"];
            return $this;
        }

        switch ($this->mode) {
            case 'r': // default queary that returns a selection of rows
                $this->result = $run->fetchAll(\PDO::FETCH_ASSOC);
                $run = null;
                $this->db = null;
                return $this;
                break;

            default: // insertion, deletion, updating
                if ($this->needLastId) {
                    $lastId = $this->db->lastInsertId();
                    $run = null;
                    //$this->db = null;
                    $this->result = $lastId;
                    return $this;
                }
                $this->result = true;
                return $this; // no need for last id
                break;
        }

        return $this;
    }

    public function get()
    {
        return $this->result;
    }
}
