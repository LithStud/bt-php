<?php
namespace App\Services;

use DI\Container;

class Orm
{
    private $db;
    private $model;
    private $container;

    public function __construct(Db $db, Container $container)
    {
        $this->db = $db;
        $this->container = $container;
    }

    public function setModel($model)
    {
        $this->model = $model;
    }

    public function select($query, $args = [])
    {
        $this->db->setMode(); // default is (r)ead;
        $this->db->setQuery($query, $args);
        return $this->db->executeQuery()->get();
    }

    public function insert($values = [])
    {
        if (empty($values)) {
            return ["error" => "Trying to insert without any values"];
        }

        $helpers = $this->container->get('App\Services\Helpers');
        $isMultiple = $helpers::isMultiArray($values);

        $this->db->setMode('insert');
        // build columns names
        $columns = '';
        foreach ($this->model->getFillable() as $col) {
            $columns .= $col . ',';
        }
        // build values identity string
        if ($isMultiple) {
            $args['pattern'] = '';
            foreach ($values as $row) {
                $args['pattern'] .= '(' . rtrim(str_repeat('?,', count($row)), ',') . '),';
            }
            $args['pattern'] = rtrim($args['pattern'], ',');
            // flatten 2D array
            $args['args'] = array_merge(...$values);
        } else {
            $args['pattern'] = '(' . rtrim(str_repeat('?,', count($values)), ',') . ')';
            $args['args'] = $values;
        }
        $query = 'INSERT INTO ' . $this->db->getCurrentDb() . '.' . $this->model->getTable()
            . ' (' . rtrim($columns, ',') . ')'
            . ' VALUES ' . $args['pattern'];
        var_dump($query);
        var_dump($args['args']);
        $this->db->setQuery($query, $args['args']);
        return $this->db->executeQuery()->get();
    }
}
