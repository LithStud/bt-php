<?php
namespace App\Services;

class View
{

    public static function render($template, $args = [])
    {
        $template = BASE_DIR . '/resources/views/' . $template . '.php';
        if (!file_exists($template)) {
            return 'Template not found.';
        }
        ob_start();
        extract($args);
        require($template);
        return ob_get_clean();
    }
}
