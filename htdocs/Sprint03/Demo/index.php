<?php
class Car
{
    private $color;
    private $engine;

    public function __construct($color = null, $engine = null)
    { 
        $this->color = $color;
        $this->engine = $engine;
    }

    /* public function __set($name, $value)
    {
        $this->$name = $value;
    }*/
    /* public function __get($name)
    {
        return $this->$name;
    } */
    public function __toString()
    {
        return 'Car color: ' . $this->color . ', engine: ' . $this->engine;
    }
}

$car = new Car('red', 'V12');
/* $car->color = 'red';
$car->engine = 'V12'; */

header("Content-Type: text/html; charset=UTF-8");
echo $car; // calls magically __toString
