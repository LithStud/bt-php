<?php
require __DIR__ . '/vendor/autoload.php';
use DI\Container;

$container = new Container();
$orm = $container->get('App\Services\Orm');

$query = "SELECT id, password FROM cms.users";

$users = $orm->select($query);

$orm->setModel($container->get('App\User'));

foreach ($users as $user) {
    $orm->update($user['id'], ['password' => password_hash($user['password'], PASSWORD_DEFAULT)]);
}
