<?php
namespace App\Controllers;

class CmsController extends AbstractController
{
    public function index()
    {
        $orm   = $this->container->get('App\Services\Orm');
        $view  = $this->container->get('App\Services\View');
        $query = "SELECT posts.id, posts.title, posts.post, posts.created, users.email FROM cms.posts
                    LEFT JOIN cms.users ON posts.user_id = users.id ORDER BY posts.created DESC";
        $posts = $orm->select($query);

        $list  = $view::render('front.html.php', compact('posts'));
        return $view::render('layout.html.php', compact('list'));
    }

    public function show($id)
    {
        $orm   = $this->container->get('App\Services\Orm');
        $view  = $this->container->get('App\Services\View');
        $query = "SELECT posts.id, posts.title, posts.post, posts.created, users.email FROM cms.posts
                    LEFT JOIN cms.users ON posts.user_id = users.id WHERE posts.id = ?";

        $posts = $orm->select($query, ['i'], [$id]);
        $list  = $view::render('post_show.html.php', compact('posts'));
        return $view::render('layout.html.php', compact('list'));
    }
}
