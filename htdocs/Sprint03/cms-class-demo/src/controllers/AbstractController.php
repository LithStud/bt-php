<?php
namespace App\Controllers;

use DI\Container;


abstract class AbstractController
{
	protected $container;

	public function __construct(Container $container)
	{
		$this->container    = $container;
	}
}
