<?php
namespace App\Controllers;

class AuthController extends AbstractController
{
	public function login()
	{
        $view = $this->container->get('App\Services\View');
        $form = $view::render('login_form.html.php');
        return $view::render('layout.html.php', compact('form'));
	}

	public function logout()
	{
        $_SESSION['auth'] = false;
        $_SESSION['user']['role'] = null;
        $_SESSION['user']['id'] = null;
        $_SESSION['user']['email'] = null;
        $_SESSION['messages']['success'][] = __('Bye');
        return redirect(BASE_URL);
	}

	public function check()
	{
        $email = '';
        $pass  = '';
        $hash = null;

        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        if (isset($_POST['email'])) {
            $email = $_POST['email'];
        }

        if (isset($_POST['password'])) {
            $pass = $_POST['password'];
        }

        $orm = $this->container->get('App\Services\Orm');
        $query = "SELECT users.id, users.password, users.role FROM cms.users WHERE users.email = ?";
        $user = $orm->select($query, ['s'], [$email]);

        if (!empty($user)) {
            $hash = $user[0]['password'];
        }

        if ($hash and password_verify($pass, $hash)) {
            $_SESSION['auth'] = true;
            $_SESSION['user']['role']  = $user[0]['role'];
            $_SESSION['user']['id']    = $user[0]['id'];
            $_SESSION['user']['email'] = $email;
            $role = 'user';
            if ($user[0]['role'] === '1') {
                $role = 'admin';
            }
            $_SESSION['messages']['success'][] = __('Welcome') . ', ' . $role;
            return redirect(BASE_URL);
        } else {
            $_SESSION['auth']          = false;
            $_SESSION['user']['role']  = null;
            $_SESSION['user']['id']    = null;
            $_SESSION['user']['email'] = null;
            $_SESSION['messages']['warning'][] = __('We don\'t have any data about this user');
            return redirect(BASE_URL . '/login');
        }
	}
}
