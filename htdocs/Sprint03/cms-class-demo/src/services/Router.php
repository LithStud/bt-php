<?php
namespace App\Services;

class Router
{
    private $protected = [];

    private $routes = [];

    public function __construct()
    {
        require __DIR__ . '/../routes/routes.php';
        $this->routes = $routes;
        $this->protected = $protected;
    }

    public function match($path, $httpMethod)
    {
        if ($path == '') {
            $path = '/';
        }

        foreach ($this->routes[$httpMethod] as $index => $route) {
            $pattern = '/' . str_replace('/', '\/', $route['_pattern']) . '/';
            if (preg_match($pattern, $path, $matches)) {
                if ($matches[0] == $path) {
                    return $this->isProtected($route);
                    break;
                }
            }
        }
        return false;
    }

    private function isProtected($route)
    {
        if (in_array($route['_name'], $this->protected)) {
            $route['_protected'] = true;
        }
        return $route;
    }
}
