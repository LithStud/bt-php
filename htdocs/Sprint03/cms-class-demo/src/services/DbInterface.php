<?php
namespace App\Services;

interface DbInterface
{
	public function get();
	public function qry();
	public function getCurrentDb();
}
