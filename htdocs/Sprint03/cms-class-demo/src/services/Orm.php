<?php
namespace App\Services;

class Orm
{
    private $db;

    private $model;

    public function __construct(Db $db)
    {
        $this->db = $db;
    }

    public function setModel($model)
    {
        $this->model = $model;
    }

    public function insert($values = [])
    {
        $types = [];
        $args = [];
        $query = "INSERT INTO " . $this->db->getCurrentDb() . '.' . $this->model->getTable() . "(";
        foreach ($this->model->getFillable() as $field) {
            if (isset($values[$field])) {
                $query .= $field . ',';
                $types[] = 's';
                $args[] = $values[$field];
            }
        }
        $query = rtrim($query, ',') . ") VALUES (" . rtrim(str_repeat('?,', count($args)), ',') . ")";
        $this->db->query = $query;
        $this->db->types = $types;
        $this->db->args  = $args;
        $this->db->mode = 'w';
        return $this->db->qry()->get();
    }

    public function delete($ids = [])
    {
        $types = [];
        $query = "DELETE FROM " . $this->db->getCurrentDb() . '.' . $this->model->getTable()
        . " WHERE " . $this->model->getTable() . '.id IN (' . rtrim(str_repeat('?,', count($ids)), ',') . ')';
        foreach ($ids as $id) {
            $types[] = 'i';
        }
        $this->db->query = $query;
        $this->db->types = $types;
        $this->db->args  = $ids;
        $this->db->mode = 'w';
        return $this->db->qry()->get();
    }

    public function update($id, $values)
    {
        $types = [];
        $args = [];
        $fillables = $this->model->getFillable();

        $query = "UPDATE  " . $this->db->getCurrentDb() . '.' . $this->model->getTable() . " SET ";

        foreach ($fillables as $fillable) {
            if (isset($values[$fillable])) {
                $query .= $fillable . '= ?,';
                $types[] = 's';
                $args[] = $values[$fillable];
            }
        }

        $query = rtrim($query, ',') . " WHERE " .  $this->model->getTable() . '.id = ?';
        $types[] = 'i';
        $args[] = $id;
        $this->db->query = $query;
        $this->db->types = $types;
        $this->db->args  = $args;
        $this->db->mode = 'w';
        return $this->db->qry()->get();
    }

    public function select($query, $types = [], $args = [])
    {
        $this->db->query = $query;
        $this->db->types = $types;
        $this->db->args = $args;
        $this->db->mode = 'r';
        return $this->db->qry()->get();
    }
}
