<?php
namespace App\Services;

class View
{
	public static function render($template, $variables = [])
	{
        $template = BASE_DIR . '/resources/views/' . $template;
        if (!file_exists($template)) {
            return 'Template not found.';
        }
        ob_start();
        extract($variables);
        include $template;
        return ob_get_clean();
	}
}
