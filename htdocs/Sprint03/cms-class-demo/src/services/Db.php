<?php
namespace App\Services;

use App\Services\DbInterface;

use Config;

class Db implements DbInterface
{
    protected $db = false;

    protected $query;

    protected $types = [];

    protected $args = [];

    protected $mode = 'r';

    protected $needLastId = true;

    protected $result;

    private $currentDb;

    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function __set($property, $value)
    {
        $this->$property = $value;
        return $this;
    }

    public function getCurrentDb()
    {
        if ($this->currentDb) {
            return $this->currentDb;
        }
        return $this->config::DB_DATA;
    }

    public function qry()
    {
        // Check if number of types and arguments is eq
        if (count($this->args) !== count($this->types)) {
            $this->result = ['fail' =>  'Query parameters count failed.'];
            return $this;
        }
        // Most used steps to perform any query to db
        $this->connect();

        if (!$this->db) {
            $this->result = ['fail' => 'DB connection failed.'];
            return $this;
        }

        $run = $this->db->prepare($this->query);

        if (!$run) {
            $this->result = ['fail' => 'Bad query.'];
            return $this;
        }

        // If we have a parameterized query, bind values
        if (!empty($this->types) and !empty($this->args)) {
            $i = 1;
            foreach ($this->types as $index => $type) {
                switch ($type) {
                    case 'i':
                    case 'integer':
                        $this->types[$index] = \PDO::PARAM_INT;
                        break;
                    case 'b':
                    case 'boolean':
                        $this->types[$index] = \PDO::PARAM_BOOL;
                        break;
                    case 'n':
                    case 'null':
                        $this->types[$index] = \PDO::PARAM_NULL;
                        break;
                    default:
                        $this->types[$index] = \PDO::PARAM_STR;
                        break;
                }
                $run->bindValue($i, $this->args[$index], $this->types[$index]);
                $i ++;
            }
        }
        // ... and execute query
        $exec = $run->execute();
        if (!$exec) {
            $this->result = ['fail' => 'Query execution failed.'];
            return $this;
        }
        // We have select
        if ($this->mode === 'r') {
            $this->result = $run->fetchAll(\PDO::FETCH_ASSOC);
            $run = null;
            $this->db = null;
            return $this;
        }
        // We have INSERT or UPDATE and need last inserted id
        if ($this->mode === 'w') {
            if ($this->needLastId) {
                $lastId = $this->db->lastInsertId();
                $run = null;
                $this->db = null;
                $this->result = $lastId;
                return $this;
            }
            // We have INSERT, UPDATE, DELETE and do not need last inserted id
            $this->result = true;
        }
        return $this;
    }

    public function get()
    {
        return $this->result;
    }

    /**
     * Connects Db PDO
     * @throws PDOException
     */
    private function connect()
    {
        $dsn  = $this->config::DB_DRIVER . ':'
            . 'host='   . $this->config::DB_HOST . ';'
            . 'port='   . $this->config::DB_PORT . ';'
            . 'dbname=' . $this->config::DB_DATA;
        try {
            @$db = new \PDO($dsn, $this->config::DB_USER, $this->config::DB_PASS,
                array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_SILENT));
        } catch (\PDOException $e) {
            // Log error here
            $this->db = false;
        }
        $this->db = $db;
        $this->currentDb = $this->config::DB_DATA;
    }
}
