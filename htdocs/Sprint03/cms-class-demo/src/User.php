<?php
namespace App;

use App\Model;

class User extends Model
{
	protected $fillable = ['email', 'password', 'role'];

    protected $table = 'users';

}
