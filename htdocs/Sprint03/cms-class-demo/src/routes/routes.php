<?php
$routes['GET'][] = [
    '_name' => 'locale',
    '_pattern' => '^/locale/(en|lt)(/|$)',
    '_controller' => 'App\Controllers\LocaleController',
    '_action' => 'change',
    '_args' => false,
];

$routes['GET'][] = [
    '_name' => 'front',
    '_pattern' => '^/$',
    '_controller' => 'App\Controllers\CmsController',
    '_action' => 'index',
    '_args' => false,
];

$routes['GET'][] = [
    '_name' => 'post_show',
    '_pattern' => '^/post/(\d+)/show(/|$)',
    '_controller' => 'App\Controllers\CmsController',
    '_action' => 'show',
    '_args' => 2,
];

$routes['GET'][] = [
    '_name' => 'posts_list',
    '_pattern' => '^/admin/post/list(/|$)',
    '_controller' => 'App\Controllers\PostsController',
    '_action' => 'list',
    '_args' => false
];

$routes['GET'][] = [
    '_name' => 'posts_add',
    '_pattern' => '^/admin/post/add(/|$)',
    '_controller' => 'App\Controllers\PostsController',
    '_action' => 'add',
    '_args' => false
];

$routes['GET'][] = [
    '_name' => 'posts_edit',
    '_pattern' => '^/admin/post/(\d+)/edit(/|$)',
    '_controller' => 'App\Controllers\PostsController',
    '_action' => 'edit',
    '_args' => 3,
];

$routes['GET'][] = [
    '_name' => 'login',
    '_pattern' => '^/login(/|$)',
    '_controller' => 'App\Controllers\AuthController',
    '_action' => 'login',
    '_args' => false,
];

$routes['GET'][] = [
    '_name' => 'logout',
    '_pattern' => '^/logout(/|$)',
    '_controller' => 'App\Controllers\AuthController',
    '_action' => 'logout',
    '_args' => false,
];

$routes['POST'][] = [
    '_name' => 'login_check',
    '_pattern' => '^/login(/|$)',
    '_controller' => 'App\Controllers\AuthController',
    '_action' => 'check',
    '_args' => false,
];


$routes['POST'][] = [
    '_name' => 'posts_save',
    '_pattern' => '^/posts/save(/|$)',
    '_controller' => 'App\Controllers\PostsController',
    '_action' => 'save',
    '_args' => false,
];

$routes['POST'][] = [
    '_name' => 'posts_trash',
    '_pattern' => '^/posts/trash(/|$)',
    '_controller' => 'App\Controllers\PostsController',
    '_action' => 'trash',
    '_args' => false,
];


$protected = [
    'posts_add',
    'posts_save',
    'posts_trash',
    'posts_edit',
    'posts_list',
];
