<?php
namespace App;

use App\ModelInterface;

class Model implements ModelInterface
{
	protected $fillable;

    protected $table;

    public function getFillable()
    {
        return $this->fillable;
    }

    public function getTable()
    {
        return $this->table;
    }
}
