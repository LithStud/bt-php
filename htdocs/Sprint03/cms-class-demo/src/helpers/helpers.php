<?php
function __($string)
{
    $file = __DIR__ . '/../../resources/translations/' . getLang() . '.json';
    if (file_exists($file)) {
        $decoded = json_decode(file_get_contents($file), true);
        return $decoded[$string] ?? $string;
    }
    return $string;
}

function getLang()
{
   if (isset($_SESSION['locale'])) {
        return $_SESSION['locale'];
    }
    if (defined('LOCALE')) {
        return LOCALE;
    }
    return setLang('en');
}

function setLang($locale)
{
    return $_SESSION['locale'] = $locale;
}

function getLocalesAvialable()
{
    return defined('LOCALES_AVAILABLE') ? LOCALES_AVAILABLE : ['en'];
}

function changeLang($locale)
{
    setLang($locale);
    return routeBack();
}

function checkLocale()
{
	// Detect last segment for language changer
	$segments = array_values(array_filter(explode('/', HTTP_PATH)));

	// Change locale (lang)
	$locale = end($segments);
	$before = prev($segments);

	if (in_array($locale, LOCALES_AVAILABLE) and $before === 'locale') {
    	   changeLang($locale);
	}
    return true;
}

function sanitizeInput()
{
    //$_GET  = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    return true;
}

function csrfToken()
{
    if (empty($_SESSION['_token'])) {
        $_SESSION['_token'] = bin2hex(random_bytes(32));
    }
}

function verifyCsrf()
{
    if (hash_equals($_SESSION['_token'], $_POST['_token'])) {
        return true;
    }
    return false;
}

// "Route helpers"
function routeBack()
{
    if (isset($_SESSION['back_route'])) {
        $route = $_SESSION['back_route'];
    }
    return redirect($route ?? BASE_URL);
}

function redirect($route, $status = 302, $replace = true)
{
    header('Location: ' . $route, $replace, $status);
    die();
}

// Auth helpers
function isAuth()
{
    return (isset($_SESSION['auth']) and $_SESSION['auth']) ? true : false;
}

function isAdmin()
{
    if (isset($_SESSION['auth']) and $_SESSION['auth'] and isset($_SESSION['user']['role']) and $_SESSION['user']['role'] === '1') {
        return true;
    }
    return false;
}
