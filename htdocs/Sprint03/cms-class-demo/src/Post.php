<?php
namespace App;

use App\Model;

class Post extends Model
{
	protected $fillable = ['title', 'post', 'user_id'];

    protected $table = 'posts';

}
