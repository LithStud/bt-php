<?php
namespace App;

interface ModelInterface
{
	public function getTable();

	public function GetFillable();
}
