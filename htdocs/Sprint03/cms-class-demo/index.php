<?php
require __DIR__ . '/vendor/autoload.php';
session_start();
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use DI\Container;

use App\Services\Router;

$request   = Request::createFromGlobals();
$router    = new Router();
$container = new Container();

$path      = $request->getPathInfo();
$method    = $request->getMethod();
$baseUrl   = $request->getBaseUrl();
$httpHost  = $request->getHost();
$httpScheme = $request->getScheme();

define ('BASE_URL', $httpScheme . '://' . $httpHost . $baseUrl);
define ('BASE_DIR', dirname(__FILE__));
define ('HTTP_PATH', $path);

// Generate CSRF token, @see helpers.php
csrfToken();

// Verify CSRF token, @see helpers.php
if ($method === 'POST') {
    if (!isset($_POST['_token']) or !verifyCsrf()) {
        $html = '<html><body><h1>' . Response::HTTP_FORBIDDEN .' Forbidden</h1></body></html>';
        $response = Response::create($html, Response::HTTP_FORBIDDEN);
        $response->send();
        die();
    }
}

if ($match = $router->match($path, $method)) {
    // Locale, @see helpers.php
    checkLocale();

    // Remember incoming get route
    if ($method === 'GET' ) {
        $_SESSION['back_route'] = BASE_URL . $path;
    }
    // Check auth
    if (isset($match['_protected'])) {
        switch (true) {
            case (!isset($_SESSION['auth'])):
            case (!$_SESSION['auth']):
            {
                redirect(BASE_URL . '/login');
                break;
            }
        }
    }

    $controller = $container->get($match['_controller']);
    $action = $match['_action'];
    $args = $match['_args'];
    if (!$args) {
        $response = Response::create($controller->$action());
    } else {
        $arg = explode('/', $path)[$match['_args']];
        $response = Response::create($controller->$action($arg));
    }
} else {
    $html = '<html><body><h1>' . Response::HTTP_NOT_FOUND .' Not Found</h1></body></html>';
    $response = Response::create($html, Response::HTTP_NOT_FOUND);
}

$response->prepare($request);
$response->send();
