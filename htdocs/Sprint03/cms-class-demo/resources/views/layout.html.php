<!DOCTYPE html>
<html lang="<?= getLang(); ?>">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?= __('The third sprint'); ?></title>
<link rel="stylesheet" href="<?= BASE_URL . '/public/css/dirs.css';?>">
<link rel="stylesheet" href="<?= BASE_URL . '/public/css/open-iconic-bootstrap.min.css';?>">
</head>
<body>


<nav class="navbar navbar-expand-lg navbar-dark bg-primary mb-2">
<?php // Left nav items ?>
<a class="navbar-brand mr-auto" href="<?= BASE_URL; ?>"><?= __('The third sprint'); ?></a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-content" aria-expanded="false" aria-label="Toggle navigation">
<span class="oi oi-ellipses"></span>
</button>

<div class="collapse navbar-collapse" id="navbar-content">
<?php // Center nav items ?>
<ul class="navbar-nav mx-auto">
<li class="nav-item"><?php if (isAuth()):?><a href="<?= BASE_URL . '/admin/post/list' ?>" class="nav-link"><?= __('Posts') ?></a><?php endif; ?></li>
</ul>

<?php // Right nav items ?>
<?php $l = BASE_URL . '/login'; $t = __('Login'); ?>
<?php if (isAuth()): ?>
<?php $l = BASE_URL . '/logout'; $t = __('Logout'); ?>
<?php endif;?>
<ul class="navbar-nav ml-auto">
<li class="nav-item"><a href="<?= $l ?>" class="nav-link"><?= $t ?></a></li>

<?php foreach ($locales = getLocalesAvialable() as $locale): ?>
<?php $active = $_SESSION['locale'] ?? getLang(); ?>

<?php if ($active === $locale) $class = ' active'; else $class = '';?>
<li class="nav-item"><a href="<?= BASE_URL .'/locale/' . $locale; ?>" class="nav-link<?= $class; ?>"><?= ucfirst($locale) ?></a></li>
<?php endforeach; ?>
</ul>
</div>
</nav>


<div class="container mb-2">
<?php include 'messages.html.php' ?>
<?= $list ?? '' ?>
<?= $form ?? '' ?>
</div>

<footer class="bg-dark">
<div class="container">

</div>
</footer>

<script defer type="text/javascript" src="<?= BASE_URL . '/public/js/dirs.js';?>"></script>
</body>
</html>

