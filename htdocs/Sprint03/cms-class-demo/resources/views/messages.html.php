<?php if (isset($_SESSION['messages'])): ?>
<?php $messages = $_SESSION['messages']; ?>
<?php unset ($_SESSION['messages']); ?>
<?php  endif; ?>
<?php $alerts = ['warning', 'danger', 'primary', 'secondary', 'success', 'info']; ?>
<?php foreach ($alerts as $alertClass): ?>
<?php if (isset($messages[$alertClass])):?>
<div class="alert alert-<?= $alertClass ?> alert-dismissible fade show">
<ul class="list-unstyled">
<?php foreach($messages[$alertClass] as $message): ?>
<li><?= $message; ?></li>
<?php endforeach;?>
</ul>
<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
</div>
<?php endif;?>
<?php endforeach; ?>
