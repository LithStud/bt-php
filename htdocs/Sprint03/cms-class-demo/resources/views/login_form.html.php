<h2><?= __('Posts') ?></h2>
<h3><?= __('Login') ?></h3>

<?php include 'messages.html.php'; ?>

<div class="row">
<div class="col-sm-12 col-md-6 col-lg-6">

<form action="<?= BASE_URL . '/login' ?>" method="POST">

<input type="hidden" name="_token" value="<?= $_SESSION['_token'] ?>">

<div class="form-group">
<label class="form-control-label"><?= __('Email') ?></label>
<input class="form-control" type="text" name="email">
</div>

<div class="form-group">
<label class="form-control-label"><?= __('Password') ?></label>
<input class="form-control" type="password" name="password">
</div>

<button class="btn btn-outline-secondary" type="submit"><?= __('Login') ?></button>
</form>
</div>
</div>
