<h2><?= __('Posts') ?></h2>

<?php include 'messages.html.php' ?>

<div class="card-columns">
<?php foreach ($posts as $post): ?>
<div class="card">
<div class="card-body">
<a class="card-link" href="<?= BASE_URL . '/post/' . $post['id'] . '/show' ?>"><h5 class="card-title"><?= $post['title']; ?></h5></a>

<p class="card-text"><small class="text-muted"><?= $post['email'] ?>, <?= $post['created']; ?></small></p>

<p class="card-text">
<?= mb_strimwidth($post['post'], 0, rand(33, 133) , " ..."); ?>
<a class="card-link" href="<?= BASE_URL . '/post/' . $post['id'] . '/show' ?>">&nbsp;&raquo;&raquo;</a>
</p>

</div>
</div>
<?php endforeach; ?>
</div>
