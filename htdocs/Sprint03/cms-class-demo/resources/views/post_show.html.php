<h2><?= __('Post') ?></h2>

<?php include 'messages.html.php' ?>

<?php foreach ($posts as $post): ?>
<div class="card">
<div class="card-body">
<h5 class="card-title"><?= $post['title']; ?></h5>
<p class="card-text"><small class="text-muted"><?= $post['email'] ?>, <?= $post['created']; ?></small></p>
<p class="card-text"><?= $post['post']; ?></p>
</div>
</div>
<?php endforeach; ?>

