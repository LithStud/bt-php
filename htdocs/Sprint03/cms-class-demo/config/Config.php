<?php
class Config
{
    const DB_DRIVER = 'mysql';
    const DB_HOST = 'localhost';
    const DB_USER = 'cms';
    const DB_DATA = 'cms_demo';
    const DB_PORT = 3306;
    const DB_PASS = 'cmsuser';
}
