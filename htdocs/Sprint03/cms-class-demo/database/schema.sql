-- utf8
SET NAMES utf8;

USE `cms`;

-- Turn off foreign key check
SET FOREIGN_KEY_CHECKS=0;

-- Table staff.departments
DROP TABLE IF EXISTS `posts`;

CREATE TABLE IF NOT EXISTS `posts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_lithuanian_ci NOT NULL DEFAULT '''''',
  `post` text COLLATE utf8_lithuanian_ci NOT NULL,
  `created` datetime DEFAULT NOW(),
  `user_id` bigint unsigned,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_uniq` (`title`)
)

ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_lithuanian_ci;

DROP TABLE IF EXISTS `users`;

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(128) COLLATE utf8_lithuanian_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `role` set('1','2','3') NOT NULL DEFAULT '3',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_uniq` (`email`)
)

ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_lithuanian_ci;

-- Turn on foreign key check;
SET FOREIGN_KEY_CHECKS=1;