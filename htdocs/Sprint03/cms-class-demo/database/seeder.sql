-- utf8
SET NAMES utf8;

USE `cms`;

-- Turn off foreign key check
SET FOREIGN_KEY_CHECKS=0;

-- Seeding table users
DELETE FROM cms.users;
INSERT INTO cms.users (`email`, `password`, `role`) VALUES
('admin@example.com', 'iddqdidkfa', '1'),
('user@example.com', 'iddqdidkfa', '2');

-- Seeding table posts
DELETE FROM cms.posts;
INSERT INTO cms.posts (`title`, `post`, `user_id`, `created`) VALUES
('Lorem', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam magna dui, suscipit a facilisis non, accumsan vel sapien. Duis dictum, quam et facilisis hendrerit, urna nisl lacinia lectus, ut tincidunt nulla magna quis dui. Suspendisse eleifend nibh id sapien consectetur consequat. Nulla urna enim, imperdiet nec ex id, pulvinar elementum lectus. Aliquam lacus magna, commodo non tristique a, tempor sed purus. Cras erat velit, consectetur at consectetur in, malesuada sed nunc. Donec vestibulum ligula id lectus luctus vestibulum. Morbi ac dolor eget elit pretium pellentesque. Mauris sodales aliquam pharetra. Nam sagittis sed augue vitae vehicula. Aliquam libero quam, aliquet quis nisi at, lacinia efficitur sem.', 1, DEFAULT),
('Duis', 'Duis dapibus ac diam at hendrerit. Nulla ultrices ac nunc nec euismod. Fusce sollicitudin viverra elit ut dictum. Sed commodo diam at placerat dignissim. Cras eget urna nec ligula bibendum luctus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis lacinia commodo ligula. Pellentesque feugiat est sed laoreet tempor. Morbi mollis dolor at lorem pulvinar tincidunt. Sed mattis dui sed gravida eleifend. Proin a sem sit amet ipsum feugiat consequat. Nunc tempus sapien eget ex eleifend porttitor.', 2, DEFAULT),
('Integer', 'Integer bibendum orci odio, et tempus tellus lacinia vel. Vestibulum non quam in urna molestie mollis. Quisque sem dui, aliquet sed vulputate a, posuere sed ex. Integer et pellentesque leo. Maecenas hendrerit ultrices diam et eleifend. Vestibulum eget nisi purus. Nullam est est, bibendum sed libero sed, dignissim lobortis lorem.', 2, DEFAULT);

-- Turn on foreign key check
SET FOREIGN_KEY_CHECKS=1;

