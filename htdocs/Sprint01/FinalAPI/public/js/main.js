//mode body tag 100% document height
document.getElementsByTagName('body')[0].style = `height: ${window.innerHeight}px`;

// for creating dom elements
let parser = new DOMParser();

// selection mode on/off
let selecting = false;

let model = {};
//load initial page
loadFolder(window.location.href);

/**
 * Renders HTML for a directory listing
 * @param {*} model Diryctory model
 */
function renderModel(model) {
    let dirEl = document.getElementsByClassName('directory_view')[0];
    dirEl.innerHTML = '';

    model.dirs.map(dir => {
        return `<a class="card folder" data-name="${dir}" href="${model.base_url + model.path + "/" + dir}">
                <img src="${model.base_url}/public/img/human-folder.svg" alt="Folder">${dir}</a>`;
    }).forEach(el => dirEl.innerHTML += el);
    model.files.map(file => {
        return `<a class="card file" data-name="${file}" href="${model.base_url + model.path + "/" + file}">
                <img src="${model.base_url}/public/img/human-ooo-writer.svg" alt="File">${file}</a>`;
    }).forEach(el => dirEl.innerHTML += el);

    if (model.dirs.length < 1 && model.files.length < 1) {
        dirEl.innerHTML += `<div>Empty Directory</div>`
    }

    // build crumbs
    let crumbEL = document.getElementsByClassName('breadcrumbs')[0];
    crumbEL.innerHTML = '';
    /**
     * @type {Array}
     */
    let crumbs = model.path.split("/").filter(Boolean);
    let path = model.base_url;
    if (crumbs.length > 0)
        crumbEL.innerHTML += `<a class="crumb" href="${path}/">ROOT</a>`; // only root needs / at the end due to how api works
    else
        crumbEL.innerHTML += `<span class="crumb">ROOT</span>`;
    crumbs.forEach((crumb, index) => {
        path += "/" + crumb;
        if (index < crumbs.length - 1)
            crumbEL.innerHTML += ` > <a class="crumb" href="${path}">${crumb}</a>`;
        else
            crumbEL.innerHTML += ` > <span class="crumb">${crumb}</span>`;
    });
}

window.addEventListener('popstate', e => {
    console.log(history.state);
    if (e.state) {
        if (e.state.base_url) {
            renderModel(e.state);
        }
    }
});

document.getElementById('createDir').addEventListener('click', e => {
    let name = prompt("New Folder name:", "NewFolder");
    console.log(name);
    //console.log(window.location);
    if (name != null) {
        //let data = { filename: name, mkdir: true };
        let form = new FormData();
        form.append("filename", name);
        form.append("mkdir", "true");
        fetch(window.location.href, {
            method: "POST",
            body: form
        })
            .then(res => { return res.json(); })
            .then(json => {
                if (json.msg) {
                    model.dirs.push(json.dir_name);
                    renderModel(model);
                } else {
                    alert(json.error);
                }
            });
    }
});

document.getElementById('delete').addEventListener('click', e => {
    selecting = !selecting;
    e.target.innerHTML = selecting ? 'Cancel' : 'Delete';
    document.getElementById('confirm_delete').classList.toggle('hidden');
    console.log("Select for deletion: ", (selecting ? "ON" : "OFF"));
    if (!selecting) { // remove all marks
        let markedEls = document.getElementsByClassName('marked');
        if (markedEls.length < 1) return;
        console.log(markedEls.length);

        for (let i = markedEls.length - 1; i >= 0; i--) {
            let el = markedEls.item(i);
            el.classList.remove('marked');
            el.getElementsByClassName('mark')[0].remove();
        }
    }
});

document.getElementById('confirm_delete').addEventListener('click', e => {
    e.preventDefault();
    e.target.classList.toggle('visible');
    let markedEls = document.getElementsByClassName('marked');
    if (markedEls.length < 1) return;

    let formData = new FormData();
    formData.append('rm', true);
    for (el of markedEls) {
        formData.append('list[]', el.dataset['name']);
    }

    fetch(window.location.href, {
        method: "POST",
        body: formData
    })
        .then(res => { return res.json(); })
        .then(json => {
            if (json.msg) {
                console.log(json);
                model.base_url = json.base_url;
                model.path = json.http_path;
                model.dirs = json.ls['DIRS'];
                model.files = json.ls['FILES'];
                renderModel(model);
                document.getElementById('delete').click();
            } else {
                alert(json.error);
            }
        });
});

document.getElementsByClassName('directory_view')[0].addEventListener('click', e => {
    e.preventDefault();
    /**
     * @type {HTMLElement}
     */
    let target = e.target.closest('a');
    if (!target) return;
    if (!selecting) { // traversing/requesting download
        if (target.classList.contains('folder')) {
            loadFolder(target.href);
        }
        else if (target.classList.contains('file')) {
            window.location.href = target.href;
        }
    } else {
        let marks = target.getElementsByClassName('mark');
        if (marks.length > 0) {
            for (mark of marks) {
                mark.remove();
            }
            target.classList.remove('marked');
        } else {
            let img = document.createElement('img');
            img.src = `${model.base_url}/public/img/del.svg`;
            img.alt = 'Marked for deletion';
            img.classList.add('mark');
            target.appendChild(img);
            target.classList.add('marked');
        }
    }
});

document.getElementsByClassName('breadcrumbs')[0].addEventListener('click', e => {
    e.preventDefault();
    /**
     * @type {HTMLElement}
     */
    let target = e.target.closest('a');
    if (target) loadFolder(target.href);
});

function loadFolder(url) {
    let form = new FormData();
    form.append("ls", "true");
    form.append("url", url);
    fetch(url, {
        method: "POST",
        body: form
    })
        .then(res => { return res.json(); })
        .then(json => {
            if (json.msg) {
                console.log(json);
                model.base_url = json.base_url;
                model.path = json.http_path;
                model.dirs = json.ls['DIRS'];
                model.files = json.ls['FILES'];
                renderModel(model);
                if (history.pushState && model != {}) { // modern browser
                    let path = model.base_url + model.path;
                    if (history.state != model)
                        history.pushState(model, '', path == model.base_url ? path + "/" : path);
                }
                //window.location.reload();
            } else {
                alert(json.error);
            }
        });
}
function ajaxSuccess(e) {
    //console.log(this.responseText);
    let json = JSON.parse(this.responseText);
    model.base_url = json.base_url;
    model.path = json.http_path;
    model.dirs = json.ls['DIRS'];
    model.files = json.ls['FILES'];
    renderModel(model);
    //loadFolder(window.location.href);
}

document.getElementById('upload').addEventListener('submit', e => {
    e.preventDefault();
    /**
     * @type {HTMLInputElement}
     */
    let filesEl = document.getElementById('file_upload_selector');
    if (filesEl.files.length > 0) {
        let oReq = new XMLHttpRequest();
        oReq.onload = ajaxSuccess;
        oReq.open("post", window.location.href);
        oReq.send(new FormData(document.getElementById('upload')));
        filesEl.value = '';
    }
});