<?php

/**
 * Lets make OS independant slashes
 */
function sanitizeSlash($url)
{
    $url = str_replace("\\", DIRECTORY_SEPARATOR, $url);
    return str_replace("/", DIRECTORY_SEPARATOR, $url);
}

session_start();

// Set protocol
$httpProtocol = 'http://';

if (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on') {
    $httpProtocol = 'https://';
}

// Set http root
$httpRoot = $httpProtocol . $_SERVER['HTTP_HOST'];

// Set http directory
$httpDir = dirname($_SERVER['SCRIPT_NAME']);

if ($httpDir == DIRECTORY_SEPARATOR) {
    $httpDir = '';
}

// Set path
$path = '';

if (isset($_SERVER['REQUEST_URI'])) {
    $path = urldecode(substr($_SERVER['REQUEST_URI'], strlen($httpDir)));
}

if ($path == '/') {
    $path = '';
}

$rootDirectory = __DIR__;

/**
 * Loading required API parts
 */
// Load config (custom constants)
require_once sanitizeSlash(__DIR__ . '/config/config.php');
// Templating functions
require_once sanitizeSlash(__DIR__ . '/src/helpers/templating.php');
// Custom Helpers
require_once sanitizeSlash(__DIR__ . '/src/helpers/customHelpers.php');
// controllers
require_once sanitizeSlash(__DIR__ . '/src/controllers/routing.php');
require_once sanitizeSlash(__DIR__ . '/src/controllers/dirs.php');
require_once sanitizeSlash(__DIR__ . '/src/controllers/files.php');

// returns what function to run according to routing rules
$function = handleRoute();

// generate response
$response = $function();

// send response
echo $response;
