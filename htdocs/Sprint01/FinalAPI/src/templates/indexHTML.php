<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" media="screen" href="<?= BASE_URL; ?>/public/css/main.css">
    <script defer src="<?= BASE_URL; ?>/public/js/main.js" type="text/javascript"></script>
    <title><?= $title; ?></title>
</head>

<body>
    <main>
        <h1><?= $title; ?></h1>
        <form id="upload" method="post" enctype="multipart/form-data">
            <input type="file" id="file_upload_selector" name="upload[]" multiple />
            <button type="submit">Upload File(s)</button>
        </form>

        <div class="delete_btn_wrapper">
            <button id="createDir">Create Dir</button>
            <button class="hidden" id="confirm_delete">Confirm</button>
            <button id="delete">Delete</button>
        </div>

        <section class="breadcrumbs">
            <!-- Node for breadcrumbs -->
        </section>
        <section class="directory_view">
            <!-- Node for directory content list -->
        </section>
    </main>
</body>

</html>