<?php
function downloadFileAction()
{
    $file = sanitizeSlash(FILES_DIR . FILE_PATH);
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="' . basename($file) . '"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));

    readfile($file);
    exit;
}

function uploadAction()
{
    $all_files = count($_FILES['upload']['tmp_name']);
    $path = sanitizeSlash(FILES_DIR . FILE_PATH);
    for ($i = 0; $i < $all_files; $i++) {
        $file_name = $_FILES['upload']['name'][$i];
        $file_tmp = $_FILES['upload']['tmp_name'][$i];
        $file_type = $_FILES['upload']['type'][$i];
        $file_size = $_FILES['upload']['size'][$i];
        //$file_ext = strtolower(end(explode('.', $_FILES['files']['name'][$i])));

        $file = $path . "/" . $file_name;
        move_uploaded_file($file_tmp, $file);
    }
    echo lsAction(); // return new dir content
    exit;
}

function rmAction()
{
    $files = [];
    if (isset($_POST['list'])) {
        $files = $_POST['list'];
        ob_start();
        foreach ($files as $key => $value) {
            $files[$key] = sanitizeSlash(FILES_DIR . FILE_PATH . '/' . $value);
            delete_files($files[$key]);
        }
        ob_end_clean();
    }
    /* return renderResponse(
        sanitizeSlash(ROOT_DIR . "/src/templates/rm.php"),
        array('files' => $files)
    ); */
    echo lsAction(); // return new dir content
    exit;
}

/* 
 * php delete function that deals with directories recursively
 * By Lewis Cowles
 */
function delete_files($target)
{
    if (is_dir($target)) {
        $files = glob($target . '*', GLOB_MARK); //GLOB_MARK adds a slash to directories returned

        foreach ($files as $file) {
            delete_files($file);
        }

        rmdir($target);
    } elseif (is_file($target)) {
        unlink($target);
    }
}
