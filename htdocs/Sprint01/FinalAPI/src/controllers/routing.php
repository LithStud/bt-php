<?php
function handleRoute()
{
    // by default display page
    $action = 'index';
    // check if its not API call
    if (file_exists(FILES_DIR . FILE_PATH)) {

        if (isset($_FILES['upload'])) {
            $action = 'upload';
        }

        if (isset($_POST['mkdir'])) {
            $action = 'mkdir';
        }

        if (isset($_POST['rm'])) {
            $action = 'rm';
        }

        if (isset($_POST['ls'])) {
            $action = 'ls';
        }

        if (is_file(FILES_DIR . FILE_PATH)) {
            $action = 'downloadFile';
        }

        // Controller's function name sufix
        $function = $action . 'Action';

        return $function;
    } else {
        // 404 Not found no need for further interaction
        echo pageNotFound();
        exit();
    }
}
