<?php
function sortDirsFirst($array)
{

    $dirs = [];
    $files = [];
    $array = array_values(array_diff($array, [".", ".."]));
    foreach ($array as $key => $value) {
        if (is_dir(FILES_DIR . FILE_PATH . DIRECTORY_SEPARATOR . $value)) {
            if (BASE_URL . FILE_PATH == BASE_URL && $value == "..") {
                continue;
            }
            $dirs[] = $value;
        } else {
            $files[] = $value;
        }
    }
    return array("DIRS" => $dirs, "FILES" => $files);
}

function indexAction()
{
    $args = array("title" => "Directory Browser");
    return renderResponse(sanitizeSlash(ROOT_DIR . "/src/templates/indexHTML.php"), $args);
}

function lsAction()
{
    $filePath = sanitizeSlash(FILES_DIR . FILE_PATH);
    $files = is_dir($filePath) ? scandir($filePath) : [];
    $files = sortDirsFirst($files);
    return renderResponse(sanitizeSlash(ROOT_DIR . "/src/templates/ls.php"), array("files" => $files));
}

function mkdirAction()
{
    $filePath = sanitizeSlash(FILES_DIR . FILE_PATH);
    $filename = $_POST['filename'];
    ob_start();
    $result = mkdir($filePath . "/" . $filename);
    ob_end_clean();
    header("Content-Type", "application/json");
    return $result ? '{"msg": "OK", "dir_name": "' . $filename . '"}' : '{"error": "Making Directory failed"}';
}
