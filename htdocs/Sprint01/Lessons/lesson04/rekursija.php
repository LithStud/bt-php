<?php 
$masyvas = [
    'coutries'=>[
        'LT'=> [
            'capital' => 'Vilnius',
            'smth' => 'Some value',
        ],
        'LV'=> [
            'capital' => 'Ryga',
            'smth' => 'Some value',
        ],
    ],
    'Index'];

function decompose($array) {
    $output = "<ul>";
    foreach($array as $key=>$value) {
        if (!is_array($value)) {
            $output .= "<li>$key: $value</li>";
        } else {
            $output .= "<li>$key: ".decompose($value)."</li>";
        }
    }
    return $output."</ul>";
}

echo decompose($masyvas);
?>