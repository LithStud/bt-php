<?php
define("TITLE", "Lesson 03");
// LOGIC GOES HERE
$json = file_get_contents("https://toast.allparcels.com/api/parcel_terminals.json?courier=LP_EXPRESS");
$data = json_decode($json, true);

function customFilter($terminal) {
    return $terminal["city"] == "Kaunas";
}

$result = array_filter($data["terminals"], "customFilter");

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=TITLE;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
</head>
<body>
    <header></header>
    <main>
    <h1><?=TITLE;?></h1>
    <?php // Context goes here ?>
    <?php print_r( $result); ?>
    </main>
    <footer></footer>
</body>
</html>