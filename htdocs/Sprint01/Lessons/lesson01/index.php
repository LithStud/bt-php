<?php
$title = "Lesson 01";
$suma = '';
$postas = '';

if (isset($_GET["k"]) && isset($_GET["l"])) {
    $suma = $_GET["k"] + $_GET["l"];
}
if (isset($_POST["user"])) {
    $postas = "a" . $_POST["user"];
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=$title;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
</head>
<body>
    <header></header>
    <main>
    <h1><?=$title;?></h1>
    <?=$suma . ($suma == '' ? '' : "<br>");?>
    <?=$postas;?>

<form action="index.php" method="POST">
<input type="text" name="user" id="user">
<button type="submit">Send!</button>
</form>
    </main>
    <footer></footer>
</body>
</html>