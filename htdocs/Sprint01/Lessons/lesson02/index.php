<?php
$title = "Lesson 02";
// LOGIC GOES HERE
$cols = 1;
$rows = 0;

if (isset($_POST["cols"])) {
    if (is_numeric($_POST["cols"])) {
        $cols = $_POST["cols"];
    }
}

if (isset($_POST["rows"])) {
    if (is_numeric($_POST["rows"])) {
        $rows = $_POST["rows"];
    }
}

$table = $rows > 0 ? "<table>" : '';

for ($row = 0; $row < $rows; $row++) {
    $table .= "<tr>";
    for ($col = 0; $col < $cols; $col++) {
        $table .= "<td>$row-$col</td>";
    }
    $table .= "</tr>";
}

$table .= $rows > 0 ? "</table>" : '';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=$title;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
</head>
<body>
    <header></header>
    <main>
    <h1><?=$title;?></h1>
    <?php // Context goes here ?>
    <form action="index.php" method="post">
        <div>
        <label for="rows">Rows: </label>
        <input type="number" name="rows"
         id="rows" placeholder="Rows"
         <?php if ($rows > 0) {echo "value='$rows'";}?>>
        </div>
        <div>
        <label for="cols">Cols: </label>
        <input type="number" name="cols"
        id="cols" placeholder="Columns"
        value="<?= $cols > 1 ? $cols : 1; ?>">
        </div>
        <button type="submit">Send</button>
    </form>
    <?= $table; ?>
    </main>
    <footer></footer>
</body>
</html>