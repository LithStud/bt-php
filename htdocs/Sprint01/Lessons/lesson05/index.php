<?php
define("TITLE", "Lesson 05");
// LOGIC GOES HERE
session_start();
$_SESSION["base"] = __DIR__;
var_dump($_SESSION);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=TITLE;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
</head>
<body>
    <header></header>
    <main>
    <h1><?=TITLE;?></h1>
    <?php // Context goes here ?>

    </main>
    <footer></footer>
</body>
</html>