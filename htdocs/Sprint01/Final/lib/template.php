<?php
function renderTemplate($template, $args)
{
    ob_start();
    extract($args);
    require($template);
    return ob_get_clean();
}