<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" media="screen" href="<?=BASE_URL;?>/resources/css/main.css">
    <script defer src="<?=BASE_URL;?>/resources/js/main.js" type="text/javascript"></script>
    <title><?=$title;?></title>
</head>

<body>
    <main>
        <h1><?=$title;?></h1>
        <button id="createDir">Create Dir</button>
        <form method="post">
            <input type="hidden" name="mkdir" value="true">
            <input type="hidden" name="filename" value="123456">
            <button type="submit">SEND!</button>
        </form>
        <section class="directory_view">
            <?php foreach ($filesArray["DIRS"] as $value): ?>
            <a draggable="true" class="card folder" href="<?=$url . "/" . $value;?>">
                <img src="<?=BASE_URL;?>/resources/img/<?= $value == ".." ? "human-go-previous.svg" : "human-folder.svg"; ?>"
                    alt="Folder">
                <?=$value == ".." ? "" : $value;?>
            </a>
            <?php endforeach;?>
            <?php foreach ($filesArray["FILES"] as $value): ?>
            <a draggable="true" class="card file" href="<?=$url . "/" . $value;?>">
                <img src="<?=BASE_URL;?>/resources/img/human-ooo-writer.svg" alt="Folder">
                <?=$value;?>
            </a>
            <?php endforeach;?>
        </section>
    </main>
</body>

</html>