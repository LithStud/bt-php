document.getElementById('createDir').addEventListener('click', e => {
    let name = prompt("New Folder name:", "NewFolder");
    console.log(name);
    //console.log(window.location);
    if (name != null) {
        //let data = { filename: name, mkdir: true };
        let form = new FormData();
        form.append("filename", name);
        form.append("mkdir", "true");
        fetch(window.location.href, {
            method: "POST",
            body: form
        })
            .then(res => { return res.json(); })
            .then(json => {
                if (json.msg) {
                    window.location.reload();
                } else {
                    alert(json.error);
                }
            });
    }
});

document.addEventListener('dragstart', e=>{
    console.log('Draggin');
    console.log(e.target);
});
document.addEventListener('dragend', e=>{
    console.log(e.target);
    console.log('Dropped drag.');
});