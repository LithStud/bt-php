<?php
session_start();
//$_POST = json_decode(file_get_contents('php://input'), true);
//var_dump(file_get_contents('php://input'));
//var_dump(file_get_contents('php://input'));
//require_once __DIR__ . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'Helpers.php';
function sanitizeSlash($url)
{
    $url = str_replace("\\", DIRECTORY_SEPARATOR, $url);
    return str_replace("/", DIRECTORY_SEPARATOR, $url);
}

function httpStatusAction()
{
    header('HTTP/1.1 404 Not Found');
    return '<h1>404</h1></br><iframe width="560" height="315" src="https://www.youtube.com/embed/wZZ7oFKsKzY?controls=0&autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
}

// Set protocol
$httpProtocol = 'http://';

if (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on') {
    $httpProtocol = 'https://';
}

// Set http root
$httpRoot = $httpProtocol . $_SERVER['HTTP_HOST'];

// Set http directory
$httpDir = dirname($_SERVER['SCRIPT_NAME']);

if ($httpDir == DIRECTORY_SEPARATOR) {
    $httpDir = '';
}

// Set path
$path = '';

if (isset($_SERVER['REQUEST_URI'])) {
    $path = urldecode(substr($_SERVER['REQUEST_URI'], strlen($httpDir)));
}

if ($path == '/') {
    $path = '';
}

$rootDirectory = __DIR__;

// Load app parts
require_once sanitizeSlash(__DIR__ . '/config/config.inc.php');
//require_once sanitizeSlash(__DIR__ . '/lib/Response.php');
require_once sanitizeSlash(__DIR__ . '/lib/template.php');
//require_once sanitizeSlash(__DIR__ . '/src/views/View.php');
//require_once sanitizeSlash(__DIR__ . '/src/controllers/DirectoryController.php');

// "Routing"
$action = 'index';
function sortDirsFirst($array)
{

    $dirs = [];
    $files = [];
    $array = array_values(array_diff($array, ["."]));
    foreach ($array as $key => $value) {
        if (is_dir(FILES_DIR . FILE_PATH . DIRECTORY_SEPARATOR . $value)) {
            if (BASE_URL . FILE_PATH == BASE_URL && $value == "..") {
                continue;
            }
            $dirs[] = $value;
        } else {
            $files[] = $value;
        }

    }
    return array("DIRS" => $dirs, "FILES" => $files);
}

function mkdirAction() {
    $filePath = sanitizeSlash(FILES_DIR . FILE_PATH);
    $filename = $_POST['filename'];
    ob_start();
    $result = mkdir($filePath . "/" . $filename);
    ob_end_clean();
    header("Content-Type", "application/json");
    //$msg = $result ? "OK" : "Making Directory failed";
    //echo indexAction();
    return $result ? '{"msg": "OK"}' : '{"error": "Making Directory failed"}';
}

function indexAction()
{
    //global $path;
    $filePath = sanitizeSlash(FILES_DIR . FILE_PATH);
    $files = is_dir($filePath) ? scandir($filePath) : [];
    $files = sortDirsFirst($files);
    $args = array("url" => BASE_URL . HTTP_PATH, "filesArray" => $files, "title" => "Directory Browser");
    return renderTemplate(__DIR__ . "/src/views/templates/index.php", $args);
}

if (file_exists(FILES_DIR . FILE_PATH)) {
    if (isset($_FILES['file_upload'])) {
        $action = 'upload';
    }

    if (isset($_POST['mkdir'])) {
        $action = 'mkdir';
    }

    if (isset($_POST['batch'])) {
        $action = 'batch';
    }

    // Controller's function name sufix
    $function = $action . 'Action';

    // Get response from controller
    $response = $function();

    // Response send
    echo $response;
} else {
    // 404 Not found
    //require_once sanitizeSlash(__DIR__ . '/src/controllers/HttpStatusController.php');
    echo httpStatusAction();
    exit();
}
