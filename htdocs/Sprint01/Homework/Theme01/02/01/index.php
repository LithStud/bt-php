<?php
/*
1.  Jonas ir Petras dalyvavo šaškių turnyre. Jonas surinko n taškų o Petras m.
Nustatykite kuris iš dalyvių surinko daugiau taškų turnyre.
Skaičiai n ir m įvedami į atskirus laukelius prie kurio nurodytas žaidėjo vardas,
laimėtojo vardas turi būti atspausdintas pilkame fone.
2.  Įvedami skaičiai -a, b, c –kraštinių ilgiai (į atskirus input laukus).
Parašykite PHP programą, kuri nustatytų, ar galima sudaryti trikampį ir rezultatą išvestų.
3. Į tris laukelius įvedamas laikas (atskirai): valandos, minutės ir sekundės.
Kiek laiko bus po vienos sekundės? (Išveskite h:m s).
4. Įmonė parduoda degtukus po 0.28EUR.
Perkant daugiau kaip už 1000 EUR taikoma 3 % nuolaida, daugiau kaip už 2000 EUR - 4 % nuolaida. Kiek eurų kainuos N degtukų?
 */
?>

<?php
define("TITLE", "Homework 02 - 01");
// LOGIC GOES HERE
// Part 01
$players = ["n"=>"Jonas", "m"=>"Petras"];
$n = '';
$m = '';
$player_winner = '';

if (isset($_POST["n"]) && isset($_POST["m"])) {
    if (is_numeric($_POST["n"]) && is_numeric($_POST["m"])) {
        $n = $_POST["n"];
        $m = $_POST["m"];
        if ($n > $m) $player_winner = $players["n"];
        elseif ($m > $n) $player_winner = $players["m"];
        else $player_winner = "Draw";
    }
}

// Part 02
$a = '';
$b = '';
$c = '';
$triangleResult = '';

function isValidTriangle($a, $b, $c) {
    return ($a + $b > $c && $a + $c > $b && $b + $c > $a);
}

if (isset($_POST["a"]) && isset($_POST["b"]) && isset($_POST["c"])) {
    if (is_numeric($_POST["a"]) && is_numeric($_POST["b"]) && is_numeric($_POST["c"])) {
        $a = $_POST["a"];
        $b = $_POST["b"];
        $c = $_POST["c"];
        if (isValidTriangle($a, $b, $c)) $triangleResult = "Legal Triangle";
        else $triangleResult = "That aint no Triangle";
    }
}

// Part 03
$hours = '';
$min = '';
$sec = '';
$timeafter = '';

// adds one sec
function windTime($hours, $min, $sec) {
    $total = $hours * 60 * 60 + $min * 60 + $sec + 1;
    $newHours = ($total - $total % 3600) / 3600;
    $total -= $newHours * 3600;
    $newMin = ($total - $total % 60) / 60;
    $newSec = $total % 60;
    return "$newHours : $newMin : $newSec";
}

if (isset($_POST["hours"]) && isset($_POST["min"]) && isset($_POST["sec"])) {
    if (is_numeric($_POST["hours"]) && is_numeric($_POST["min"]) && is_numeric($_POST["sec"])) {
        $hours = $_POST["hours"];
        $min = $_POST["min"];
        $sec = $_POST["sec"];
        $timeafter = windTime($hours, $min, $sec);
    }
}

// Part 04
$matches = '';
$totalCost = '';
$totalDiscount = '';
$cost = 0.28;

function getDiscount($value) {
    switch (true) {
        case ($value >= 1000 && $value < 2000):
            return 0.03; // 3% discount
        case ($value >= 2000):
            return 0.04; // 4%
    }
    return 0;
}

if (isset($_POST["matches"]) && is_numeric($_POST["matches"])) {
    $matches = $_POST["matches"];
    $totalCost = $matches * $cost;
    $discount = getDiscount($totalCost);
    $totalDiscount = $totalCost * $discount;
    $totalCost -= $totalDiscount;
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=TITLE;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
</head>

<body>
    <header></header>
    <main>
        <h1><?=TITLE;?></h1>
        <?php // Context goes here ?>
        <div class="players">
            <h3>Checkers</h3>
            <form action="index.php" method="post">
                <div>
                    <label for="n"><?= $players["n"]; ?>: </label>
                    <input type="number" name="n" id="jonasN" value="<?= ($n != '' ? $n : '')?>">
                </div>
                <div>
                    <label for="m"><?= $players["m"]; ?>: </label>
                    <input type="number" name="m" id="petrasM" value="<?= ($m != '' ? $m : '')?>">
                </div>
                <button type="submit">Who Won?</button>
            </form>
            <?php if ($player_winner != ''): ?>
            <div class="result">
                <?= $player_winner; ?>
            </div>
            <?php endif; ?>
        </div>

        <div class="triangle">
            <h3>Triangle Check</h3>
            <form action="index.php" method="post">
                <div>
                    <label for="a">A: </label>
                    <input type="number" min="0" name="a" id="a" value="<?= ($a != '' ? $a : '')?>">
                </div>
                <div>
                    <label for="b">B: </label>
                    <input type="number" min="0" name="b" id="b" value="<?= ($b != '' ? $b : '')?>">
                </div>
                <div>
                    <label for="c">C: </label>
                    <input type="number" min="0" name="c" id="c" value="<?= ($c != '' ? $c : '')?>">
                </div>
                
                <button type="submit">Triangle?</button>
            </form>
            <?php if ($triangleResult != ''): ?>
            <div class="result">
                <?= $triangleResult; ?>
            </div>
            <?php endif; ?>
        </div>

        <div class="time">
            <h3>Time</h3>
            <form action="index.php" method="post">
                <div>
                    <label for="hours">Hours: </label>
                    <input type="number" min="1" max="24" name="hours" id="hours" value="<?= ($hours != '' ? $hours : '')?>">
                </div>
                <div>
                    <label for="min">Minutes: </label>
                    <input type="number" min="1" max="60" name="min" id="min" value="<?= ($min != '' ? $min : '')?>">
                </div>
                <div>
                    <label for="sec">Seconds: </label>
                    <input type="number" min="1" max="60" name="sec" id="sec" value="<?= ($sec != '' ? $sec : '')?>">
                </div>
                
                <button type="submit">After 1s?</button>
            </form>
            <?php if ($timeafter != ''): ?>
            <div class="result">
                <?= $timeafter; ?>
            </div>
            <?php endif; ?>
        </div>

        <div class="matches">
            <h3>Matches - Discount</h3>
            <form action="index.php" method="post">
                <div>
                    <label for="matches">MatchBox Total: </label>
                    <input type="number" min="0" name="matches" id="matches" value="<?= ($matches != '' ? $matches : '')?>">
                </div>
                
                <button type="submit">Total Cost?</button>
            </form>
            <?php if ($totalCost != ''): ?>
            <div class="result">
                Cost per One: <?= $cost; ?>&euro;<br>
                Discount: <?= ($discount * 100); ?>% (<?=$totalDiscount;?>&euro;)<br>
                Total Cost: <?= $totalCost; ?>&euro;
            </div>
            <?php endif; ?>
        </div>

    </main>
    <footer></footer>
</body>

</html>