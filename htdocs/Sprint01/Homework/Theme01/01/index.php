<?php
$title = "Lesson 01 Homework";
// LOGIC GOES HERE

$mass = 0;
$height = 0;
$BMI = 0;

if (isset($_POST["mass"]) && isset($_POST["height"])) {
    if (is_numeric($_POST["mass"]) and $_POST["mass"] > 0) {
        $mass = $_POST["mass"];
    }
    if (is_numeric($_POST["height"]) && $_POST["height"] > 0) {
        $height = $_POST["height"] / 100;
    }
}

if ($mass != 0 && $height != 0) {
    $BMI = round($mass / pow($height, 2), 2);
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=$title;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
</head>
<body>
    <header></header>
    <main>
    <h1><?=$title;?></h1>
    <?php // Context goes here ?>
    <form action="index.php" method="post">
        <div class="height">
        <label for="height">Height: </label>
        <input type="number" name="height"
         id="height" placeholder="Height (cm)"
         <?php if ($height > 0) {echo "value='" . ($height * 100) . "'";}?>>
         <label for="height">cm</label>
        </div>
        <div>
        <label for="mass">Mass: </label>
        <input type="number" name="mass"
        id="mass" placeholder="Mass (kg)"
        <?php if ($mass > 0) {echo "value='$mass'";}?>>
        <label for="mass">kg</label>
        </div>
        <button type="submit">Send</button>
    </form>
    <?php if ($BMI > 0) {?>
    <div class="result">
    
    <div>Your result is:</div>
    <?php
}

switch (true) {
    case (0 < $BMI && $BMI < 18.5):
        echo "$BMI - Underweight";
        break;
    case (18.5 <= $BMI && $BMI < 24.9):
        echo "$BMI - Normal Weight";
        break;
    case (25 <= $BMI && $BMI < 29.9):
        echo "$BMI - Overweight";
        break;
    case (30 <= $BMI && $BMI < 34.9):
        echo "$BMI - Weak Obesity";
        break;
    case (35 <= $BMI && $BMI < 39.9):
        echo "$BMI - Middle Obesity";
        break;
    case ($BMI > 40):
        echo "$BMI - Severe Obesity";
        break;
}
?>
    </div>
    </main>
    <footer></footer>
</body>
</html>