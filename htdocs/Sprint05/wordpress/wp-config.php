<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp' );

/** MySQL database username */
define( 'DB_USER', 'wp' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wpuser' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'S/0rp?hJ!m6PQ_~8&xsYo#nE9YNXh>:f}~Tlq|{0huE8MyX)|4-dMd aK.ZlP%@8' );
define( 'SECURE_AUTH_KEY',  '}kc#4mRK.PUwVF{k^2pL=UQv{Y%FL39@89h8[cQY#`kxdde7%7<F6ul~T<`JjAnH' );
define( 'LOGGED_IN_KEY',    'zjF*UQ/QZc#]g]W=Ov%z5=t,qdx]T$Oh5e!3ee-m[WM{h|??{.}XC~d]]&:YU)V[' );
define( 'NONCE_KEY',        '< :g/3$^lH#@,*KITn8gTRX^#[a3EQ]>w15Tx.65>r4?KhJNA]Bno_*R22mOzg}0' );
define( 'AUTH_SALT',        'HsmMOekhlUoaF_Hji8,D~9]JxKK:^%3n(!/|,207f<j-3t`$q`Fli2O+| :$t7: ' );
define( 'SECURE_AUTH_SALT', '6-Y0)2~id<W_2.Kk!{{@|f)5wL?F|=89}W~1,oix+x1++HdP@UH{|bL> O&a|R%B' );
define( 'LOGGED_IN_SALT',   'b#[FHXkC0bj+uCPc%<oFqH/aa_#zg*!3S~OAqM[C636}86_}7ecqut4qAPlMaioQ' );
define( 'NONCE_SALT',       '~9g`a9oO(]Gwqs(I77l%a>nB9nGJ[m6u[o6Z5@;Hh^r!N=wZnMFqia[M54T~:.Pe' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
