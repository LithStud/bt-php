<?php
/**
 * @package Product_Type
 * @version 1.0.0
 */
/*
Plugin Name: Product Type
Description: Custom post for products.
Author: LithStud
Version: 1.0.0
*/

function product_type_taxonomy()
{
    $labels = array(
        'name' => 'Product type taxonomy'
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_nav_items' => true,
        'show_tagcloud' => true,
    );

    register_taxonomy('product_types', array('product_type', 'post'), $args);
}
add_action('init', 'product_type_taxonomy', 0);



function product_type_post()
{
    $labels = array(
        'name' => 'Product type post'
    );

    $args = array(
        'label' => 'Product type',
        'description' => 'Product type description',
        'labels' => $labels,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
        'taxonomies' => array('post_tag'),
        'menu_position' => 5,
        'thumbnails' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post',
    );
    register_post_type('product_type', $args);
}

add_action('init', 'product_type_post', 0);
