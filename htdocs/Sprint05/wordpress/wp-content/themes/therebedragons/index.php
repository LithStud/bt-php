<?php
/**
 * The main template file
 */

get_header();

if (have_posts()) :
    while (have_posts()) :
        the_post(); ?>
        <div class="card mb-3">
            <div class="card-body">
                <h5 class="card-title"><?= the_title(); ?></h5>
                <p class="card-text"><?= the_excerpt(); ?></p>
                <a href="<?= the_permalink(); ?>" class="btn btn-primary">More</a>
            </div>
        </div>
    <?php
endwhile;

endif; ?>

<?php
get_footer();
