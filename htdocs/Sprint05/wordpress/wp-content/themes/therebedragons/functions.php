<?php

remove_action('wp_head', 'wp_generator');

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');

function therebedragons_add_css()
{
    wp_enqueue_style(
        'basecss',
        get_template_directory_uri() . '/css/app.css',
        array(),
        false,
        false
    );
}

add_action('get_header', 'therebedragons_add_css');

function therebedragons_add_js()
{
    wp_enqueue_script(
        'basejs',
        get_template_directory_uri() . '/js/app.js',
        array(),
        false,
        false
    );
}

add_action('get_footer', 'therebedragons_add_js');

function therebedragons_register_nav()
{
    register_nav_menu('custom_menu', 'Your custom menu');
}

add_action('init', 'therebedragons_register_nav');

function therebedragons_get_nav_items()
{
    $menu_locations = get_nav_menu_locations();
    $menu_arr = array();

    if (isset($menu_locations['custom_menu'])) {
        $menu_arr = wp_get_nav_menu_items($menu_locations['custom_menu']);
    }
    
    return $menu_arr;
}

function therebedragons_nav_menu()
{
    $items = therebedragons_get_nav_items();

    if (empty($items)) {
        return false;
    }
    
    ob_start();
    require __DIR__ . '/template-parts/nav-menu/therebedragons-menu.php';
    ob_end_flush();
}
