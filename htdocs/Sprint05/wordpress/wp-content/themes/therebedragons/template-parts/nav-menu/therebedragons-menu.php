<?php

foreach ($items as $item) : ?>
    <li class="nav-item">
        <a class="nav-link" href="<?= esc_url($item->url); ?>"><?= $item->title; ?></a>
    </li>
<?php endforeach;
